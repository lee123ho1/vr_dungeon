using Scenes.AI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class otherScript : MonoBehaviour, IDamagable
{
    [SerializeField] private Collider _collider;
    private bool firstFrame = false;

    public float Hp => throw new System.NotImplementedException();

    public float MaxHp => throw new System.NotImplementedException();

    public void Damage(float damageAmount)
    {
        
    }

    private void Update()
    {
        if(!firstFrame)
        {
            _collider.enabled = true;
            firstFrame = true;
        }
    }
}
