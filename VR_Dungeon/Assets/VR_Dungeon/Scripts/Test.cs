using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    public Transform tar;
    public Transform goldBug;
    public Land _land;
    public Canvas end;

    private void Update()
    {
        if (_land._isLight)
            transform.position = Vector3.MoveTowards(transform.position, tar.position, 0.1f * Time.deltaTime);

        if ((transform.position - goldBug.position).magnitude < 0.15f)
        {
            gameObject.SetActive(false);
            end.gameObject.SetActive(true);
        }
    }
}
