using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class Gem : MonoBehaviour
{
    [SerializeField] private List<Collider> _sockets = new List<Collider>();
    [SerializeField] private List<GameObject> _gems = new List<GameObject>();

    [SerializeField] private bool Gem01, Gem02;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("hit");
        if (other.transform.tag == "checker")
        {
            if ((_sockets[0].transform.position - other.transform.parent.transform.position).magnitude > (_sockets[1].transform.position - other.transform.parent.transform.position).magnitude
                && Gem02 == false)
            {
                other.transform.parent.gameObject.SetActive(false);
                _sockets[1].enabled = false;
                _gems[1].SetActive(true);
                Gem02 = true;

                client c = GameObject.Find("Server").GetComponent<client>();

                cs_packet_map3_gem p = new cs_packet_map3_gem();
                p.size = (char)Marshal.SizeOf(typeof(cs_packet_map3_gem));
                p.type = (char)Packet.CS_PACKET_MAP3_GEM;
                p.side = false;

                c.send(Packet.serailize(p));
            }
            else if ((_sockets[0].transform.position - other.transform.parent.transform.position).magnitude < (_sockets[1].transform.position - other.transform.parent.transform.position).magnitude
                && Gem01 == false)
            {
                other.transform.parent.gameObject.SetActive(false);
                _sockets[0].enabled = false;
                _gems[0].SetActive(true);
                Gem01 = true;

                client c = GameObject.Find("Server").GetComponent<client>();

                cs_packet_map3_gem p = new cs_packet_map3_gem();
                p.size = (char)Marshal.SizeOf(typeof(cs_packet_map3_gem));
                p.type = (char)Packet.CS_PACKET_MAP3_GEM;
                p.side = true;

                c.send(Packet.serailize(p));
            }
        }
    }
}