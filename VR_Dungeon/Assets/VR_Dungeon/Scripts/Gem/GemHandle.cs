using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemHandle : MonoBehaviour
{
    [SerializeField] private List<GameObject> gameObjects = new List<GameObject>();
    private bool firstGem = false;

    private void Start()
    {
        gameObjects.Add(GameObject.Find("Slot2"));
        gameObjects.Add(GameObject.Find("Slot3"));
    }

    public void getGem()
    {
        var point = GetComponent<LockToPoint>();

        if (gameObjects[0].GetComponent<Slot>()._onItem == 0)
        {
            gameObjects[0].GetComponent<Slot>()._onItem = 1;
            firstGem = true;
        }

        if (firstGem)
            point.snapTo = gameObjects[0].transform;
        else
            point.snapTo = gameObjects[1].transform;
    }
}
