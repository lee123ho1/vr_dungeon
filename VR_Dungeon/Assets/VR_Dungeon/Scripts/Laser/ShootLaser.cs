using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class ShootLaser : MonoBehaviour
{
    public Material material;
    public GameObject Gem;
    public Transform gemPoint;
    public LayerMask layer;

    private LaserBeam _laserBeam;
    private bool spawn = false;

    // Update is called once per frame
    void Update()
    {
        Destroy(GameObject.Find("Laser Beam"));
        _laserBeam = new LaserBeam(transform.position, transform.forward, material, layer);
        if (_laserBeam.gem && !spawn)
        {
            GameObject g = Instantiate(Gem);
            g.transform.position = gemPoint.position;
            g.tag = "ServerGem";
            spawn = true;
            client c = GameObject.Find("Server").GetComponent<client>();
            if (c.isHost)
            {
                cs_packet_map4_clear p = new cs_packet_map4_clear();
                p.size = (char)Marshal.SizeOf(typeof(cs_packet_map4_clear));
                p.type = (char)Packet.CS_PACKET_MAP4_CLEAR;

                c.send(Packet.serailize(p));
            }
        }
    }
}