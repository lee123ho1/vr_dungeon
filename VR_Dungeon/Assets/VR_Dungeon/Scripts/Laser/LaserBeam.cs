using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBeam
{
    Vector3 _pos, _dir;

    public GameObject Gem;

    private GameObject _laserObject;
    private LineRenderer _laser;
    private List<Vector3> _laserIndices = new List<Vector3>();

    public bool gem = false;

    public LaserBeam(Vector3 pos, Vector3 dir, Material material, LayerMask layer)
    {
        _laser = new LineRenderer();
        _laserObject = new GameObject();
        _laserObject.name = "Laser Beam";
        _pos = pos;
        _dir = dir;

        _laser = _laserObject.AddComponent<LineRenderer>();
        _laser.startWidth = 0.01f;
        _laser.endWidth = 0.01f;
        _laser.material = material;
        _laser.startColor = Color.white;
        _laser.endColor = Color.white;

        CastRay(pos, dir, _laser, layer);
    }

    private void CastRay(Vector3 pos, Vector3 dir, LineRenderer laser, LayerMask layer)
    {
        _laserIndices.Add(pos);

        Ray ray = new Ray(pos, dir);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 30, layer))
        {
            CheckHit(hit, dir, _laser, layer);
        }
        else
        {
            _laserIndices.Add(ray.GetPoint(30));
            UpdateLaser();
        }
    }

    private void CheckHit(RaycastHit hitInfo, Vector3 direction, LineRenderer laser, LayerMask layer)
    {
        if (hitInfo.collider.gameObject.CompareTag("checker"))
        {
            gem = true;
        }

        if(hitInfo.collider.gameObject.CompareTag("Mirror"))
        {
            Vector3 pos = hitInfo.point;
            Vector3 dir = Vector3.Reflect(direction, hitInfo.normal);

            CastRay(pos, dir, laser, layer);
        }
        else
        {
            _laserIndices.Add(hitInfo.point);
        }   UpdateLaser();
    }

    void UpdateLaser()
    {
        int count = 0;
        _laser.positionCount = _laserIndices.Count;

        foreach (Vector3 idx in _laserIndices)
        {
            _laser.SetPosition(count, idx);
            count++;
        }
    }
}
