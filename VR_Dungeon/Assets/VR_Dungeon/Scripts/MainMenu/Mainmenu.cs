using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Mainmenu : MonoBehaviour
{
    public TMP_InputField _inputField;
    public Image _ipPanel;
    public Toggle _hostToggle;
    public IP _ip;

    public void onStart()
    {
        _ipPanel.gameObject.SetActive(true);
    }

    public void onJoin()
    {
        Debug.Log(_inputField.text);
        _ip.ip = _inputField.text;
        _ip.isHost = _hostToggle.isOn;

        SceneManager.LoadScene("map2");
    }

    public void onTest()
    {
        Debug.Log(_inputField.text);
        _ip.ip = _inputField.text;
    }

    public void GameExit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
