using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IP : MonoBehaviour
{
    public static IP instanceIP;
    public string ip = default;
    public bool isHost = default;

    private void Awake()
    {
        instanceIP = this;

        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        Debug.Log("start");
    }
}
