using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class InPlayMenu : MonoBehaviour
{
    [SerializeField] private GameObject _pointer;
    [SerializeField] private Canvas _canvas;
    [SerializeField] private SteamVR_Action_Boolean _actionBoolean;
    [SerializeField] private SteamVR_Behaviour_Pose _pose = null;

    [SerializeField] private bool activeMenu;

    void Update()
    {
        if (_actionBoolean.GetStateDown(_pose.inputSource))
        {
            if (!activeMenu)
            {
                MenuActive();
            }
            else
            {
                MenuDeactive();
            }
        }
    }

    private void MenuDeactive()
    {
        _pointer.SetActive(false);
        _canvas.gameObject.SetActive(false);
        activeMenu = false;
    }

    private void MenuActive()
    {
        _pointer.SetActive(true);
        _canvas.gameObject.SetActive(true);
        activeMenu = true;
    }

    public void onResume()
    {
        MenuDeactive();
    }

    public void onExit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
