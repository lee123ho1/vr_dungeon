using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLocationSet3 : MonoBehaviour
{
    void Start()
    {
        var player = GameObject.Find("VRController");
        if (player.GetComponent<Player>().prevScene == 2)
            player.transform.position = transform.position;
        else if (player.GetComponent<Player>().prevScene == 4)
            player.transform.position = transform.GetChild(0).position;
        else if (player.GetComponent<Player>().prevScene == 5)
            player.transform.position = transform.GetChild(1).position;
    }
}
