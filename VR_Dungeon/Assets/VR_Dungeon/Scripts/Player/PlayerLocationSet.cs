using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerLocationSet : MonoBehaviour
{
    void Start()
    {
        var player = GameObject.Find("VRController");
        if (SceneManager.GetActiveScene().buildIndex == 1)
            player.GetComponent<Player>().prevScene = 2;
        else if (SceneManager.GetActiveScene().buildIndex == 3)
            player.GetComponent<Player>().prevScene = 4;
        else if (SceneManager.GetActiveScene().buildIndex == 4)
            player.GetComponent<Player>().prevScene = 5;
        player.transform.position = transform.position;
    }
}
