using Scenes.AI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;

public class Player : MonoBehaviour, IDamagable
{
    [SerializeField] private Stat stat;
    public float prevScene;
    public Image bloodScreen;
    private Coroutine _coroutine;


    public float Hp => stat.Hp;

    public float MaxHp => stat.MaxHp;

    public void Damage(float damageAmount)
    {
        stat.AddHp(-damageAmount);
        _coroutine = StartCoroutine(BloodScreenActive(5));
        if (stat.Hp <=0)
        {

        }
    }

    public IEnumerator BloodScreenActive(int num)
    {
        int count = 0;

        while (count < num)
        {
            bloodScreen.color += new Color(0f, 0f, 0f, 0.2f);
            count++;
            yield return new WaitForFixedUpdate();
        }

        yield return new WaitForSeconds(0.5f);
        count = 0;

        while (count < num)
        {
            bloodScreen.color -= new Color(0f, 0f, 0f, 0.2f);
            count++;
            yield return new WaitForFixedUpdate();
        }
        StopCoroutine(_coroutine);
    }
}
