using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodScreen : MonoBehaviour
{
    public Canvas bloodScreen;
    private Coroutine _coroutine;

    public IEnumerator BloodScreenActive(int num)
    {
        int count = 0;

        while (count < num)
        {
            count++;
            yield return new WaitForFixedUpdate();
        }

        yield return new WaitForSeconds(0.5f);
        count = 0;

        while (count < num)
        {
            count++;
            yield return new WaitForFixedUpdate();
        }
        StopCoroutine(_coroutine);
    }
}
