using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitTorch : MonoBehaviour
{
    public bool _isLight = false;
    public Collider _other;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("gear") && !_isLight)
        {
            _isLight = true;
            _other = other;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("gear") && _isLight)
        {
            _isLight = false;
            _other = null;
        }
    }
}
