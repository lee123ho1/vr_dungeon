using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BlueBeetle : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private Transform _planet;
    [SerializeField] private float _rotationSpeed;

    private Land _land;
    private Rigidbody _rigidbody;
    private NavMeshAgent _agent;
    private bool _isPuzzle;
    private bool _isHandle;
    private float originHeight;
    private float timer;
    private float wanderTime;

    private void Start()
    {
        _land = _planet.GetComponent<Land>();
        _rigidbody = GetComponentInParent<Rigidbody>();
        _agent = GetComponentInParent<NavMeshAgent>();

        originHeight = transform.parent.position.y;
        wanderTime = Random.Range(2, 5);
        timer = wanderTime;
    }

    //private void Update()
    //{
    //    timer += Time.deltaTime;

    //    if (_isHandle)
    //    {
    //        _agent.enabled = false;
    //        _rigidbody.isKinematic = true;
    //    }
    //    else if (!_isHandle)
    //    {
    //        _rigidbody.isKinematic = true;
    //    }
    //}

    void FixedUpdate()
    {
        if (_isPuzzle)
        {
            //_rigidbody.AddForce((transform.position - _planet.position).normalized * -9.8f);

            if (_land.IsLight)
            {
                //transform.rotation = Quaternion.LookRotation(_land.HitPos);
                //transform.position = Vector3.Lerp(transform.position, _land.HitPos, 0.001f);
                if ((transform.position - _land.HitPos).magnitude > 0.05f)
                {
                    //transform.Translate(-(transform.position - _land.HitPos).normalized * 0.01f, Space.World);
                    //transform.Translate(-(transform.position - _land.HitPos).normalized * 0.01f);
                    transform.position = Vector3.MoveTowards(transform.position, _land.HitPos, 0.01f);
                }

                if ((transform.position - _target.position).magnitude < 0.15f)
                {
                    transform.rotation = Quaternion.LookRotation(_target.position);
                    gameObject.SetActive(false);
                }

                //transform.LookAt(_land.HitPos);
                //transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.FromToRotation(transform.up,
                //    (transform.position - _planet.position).normalized) * transform.rotation, _rotationSpeed);
            }
        }
        else if (!_isHandle)
        {
            if (timer >= wanderTime)
            {
                Vector3 newPos = RandomNavSphere(transform.position, 30f, -1);
                _agent.SetDestination(newPos);
                timer = 0;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("checker"))
        {
            other.gameObject.SetActive(false);
            _rigidbody.useGravity = true;
            _rigidbody.isKinematic = false;
            transform.position = other.transform.GetChild(0).transform.position;
            transform.rotation = other.transform.GetChild(0).transform.rotation;
            transform.parent = null;
            _isPuzzle = true;
        }
    }

    public Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {
        NavMeshHit navHit;

        while (true)
        {
            Vector3 randDirection = Random.insideUnitSphere * dist;
            randDirection += origin;

            NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

            if (navHit.mask == 8)
                break;
            else
                continue;
        }

        return navHit.position;
    }

    public void isHandle()
    {
        _isHandle = !_isHandle;
    }
}