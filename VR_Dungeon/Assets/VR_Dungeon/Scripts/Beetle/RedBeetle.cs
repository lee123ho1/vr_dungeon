using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AI;

public class RedBeetle : MonoBehaviour
{
    [SerializeField] private Transform planet;
    [SerializeField] private float wanderRad;
    [SerializeField] GameObject prefab;
    private NavMeshAgent _agent;
    private EntityManager _entityManager;
    private Entity _instance;
    private BlobAssetStore _blobAssetStore;
    private float timer;
    private float wanderTime;
    private Vector3 _startPos;

    void Start()
    {
        _agent = transform.GetComponent<NavMeshAgent>();

        wanderTime = Random.Range(2f, 5f);
        timer = wanderTime;
        _startPos = transform.position;

        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, _blobAssetStore);
        var prefabECS = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, settings);
        _entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

        _instance = _entityManager.Instantiate(prefabECS);
    }

    void Update()
    {
        timer += Time.deltaTime;

        //if (_land.IsLight)
        //{
        //    if ((_land.HitPos - transform.position).magnitude < 5f)
        //    {
        //        _agent.SetDestination(_land.HitPos);
        //    }
        //    else
        //    {
        //        if (timer >= wanderTime)
        //        {
        //            Vector3 newPos = RandomNavSphere(transform.position, wanderRad, -1);
        //            _agent.SetDestination(newPos);
        //            timer = 0;
        //        }
        //    }
        //}
        //else
        //{
        if (timer >= wanderTime)
        {
            Vector3 randomPos = RandomNavSphere(_startPos, wanderRad, -1);
            _agent.SetDestination(randomPos);
            Quaternion rotation = Quaternion.LookRotation((randomPos - transform.position).normalized);
            _entityManager.SetComponentData(_instance, new Rotation { Value = rotation });
            timer = 0;
        }
        //}

        //transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.FromToRotation(transform.up,
        //    (transform.position - planet.position).normalized) * transform.rotation, 40f * Time.deltaTime);

        _entityManager.SetComponentData(_instance, new Translation { Value = transform.position });
    }

    public Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {
        NavMeshHit navHit;

        Vector3 randDirection = Random.insideUnitSphere * dist;
        randDirection += origin;

        NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

        return navHit.position;
    }

    private void OnDestroy()
    {
        if (_blobAssetStore != null)
            _blobAssetStore.Dispose();
    }
}
