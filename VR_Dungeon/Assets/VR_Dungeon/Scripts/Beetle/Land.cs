using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class Land : MonoBehaviour
{
    public Transform sphere;
    [SerializeField] LayerMask _layerMask;
    public HitTorch collider;
    private Transform _torch;
    private Vector3 _hitPos;
    public bool _isLight = false;
    public Vector3 HitPos => _hitPos;
    public bool IsLight => _isLight;
    private bool master = false;
    private bool enter = false;
    private client c;

    private void Start()
    {
        c = GameObject.Find("Server").GetComponent<client>();
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.CompareTag("gear") && !_isLight)
    //    {
    //        _isLight = true;
    //        _torch = other.gameObject.transform;
    //        master = true;

    //        cs_packet_enter_torch p = new cs_packet_enter_torch();
    //        p.size = (char)Marshal.SizeOf(typeof(cs_packet_enter_torch));
    //        p.type = (char)Packet.CS_PACKET_ENTER_TORCH;
    //        p.enter = true;

    //        c.send(Packet.serailize(p));
    //    }
    //}

    private void Update()
    {
        if(collider._isLight && !enter)
        {
            _isLight = true;
            _torch = collider._other.gameObject.transform;
            master = true;

            cs_packet_enter_torch p = new cs_packet_enter_torch();
            p.size = (char)Marshal.SizeOf(typeof(cs_packet_enter_torch));
            p.type = (char)Packet.CS_PACKET_ENTER_TORCH;
            p.enter = true;

            c.send(Packet.serailize(p));
            enter = true;
        }
        else if(!collider._isLight && enter)
        {
            _isLight = false;
            master = false;

            cs_packet_enter_torch p = new cs_packet_enter_torch();
            p.size = (char)Marshal.SizeOf(typeof(cs_packet_enter_torch));
            p.type = (char)Packet.CS_PACKET_ENTER_TORCH;
            p.enter = false;

            c.send(Packet.serailize(p));
            enter = false;
        }

        if (_isLight && master)
        {
            //Debug.DrawRay(_torch.position, (transform.position - _torch.position).normalized * 5, Color.blue, 0.3f);
            Physics.Raycast(_torch.position, (transform.position - _torch.position).normalized, out RaycastHit hit, 5f, _layerMask);
            _hitPos = hit.point;

            sphere.position = _hitPos;

            cs_packet_torch_pos p = new cs_packet_torch_pos();
            p.size = (char)Marshal.SizeOf(typeof(cs_packet_torch_pos));
            p.type = (char)Packet.CS_PACKET_TORCH_POS;
            p.x = _hitPos.x;
            p.y = _hitPos.y;
            p.z = _hitPos.z;

            c.send(Packet.serailize(p));
        }
    }

    //private void OnTriggerExit(Collider other)
    //{
    //    if (other.CompareTag("gear") && _isLight && master)
    //    {
    //        _isLight = false;
    //        master = false;

    //        cs_packet_enter_torch p = new cs_packet_enter_torch();
    //        p.size = (char)Marshal.SizeOf(typeof(cs_packet_enter_torch));
    //        p.type = (char)Packet.CS_PACKET_ENTER_TORCH;
    //        p.enter = false;

    //        c.send(Packet.serailize(p));
    //    }
    //}
}