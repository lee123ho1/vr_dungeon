using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollChanger : MonoBehaviour
{
    [SerializeField] private GameObject characterObj;
    [SerializeField] private GameObject ragdollObj;
    [SerializeField] private List<Collider> _colliders = new List<Collider>();
    [SerializeField] private SkinnedMeshRenderer _meshRenderer;

    private Coroutine _ragdollDisable;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            ChangeRagdoll();
    }

    public void ChangeRagdoll()
    {
        CopyCharacterTransformToRagdoll(characterObj.transform, ragdollObj.transform);

        foreach (var _collider in _colliders)
            _collider.enabled = false;
        _meshRenderer.enabled = false;
        ragdollObj.SetActive(true);
        _ragdollDisable = StartCoroutine(RagdollDisable());
    }

    private IEnumerator RagdollDisable()
    {
        yield return new WaitForSeconds(5f);

        var capsuleColls = transform.GetComponentsInChildren<CapsuleCollider>();
        var rigidbodies = transform.GetComponentsInChildren<Rigidbody>();

        foreach (var coll in capsuleColls)
        {
            coll.enabled = false;
        }

        foreach (var rigidbody in rigidbodies)
        {
            rigidbody.isKinematic = true;
        }

        StopCoroutine(_ragdollDisable);
    }

    private void CopyCharacterTransformToRagdoll(Transform origin, Transform ragdoll)
    {
        for (int i = 0; i < ragdoll.childCount; i++)
        {
            if(origin.childCount != 0)
            {
                CopyCharacterTransformToRagdoll(origin.GetChild(i), ragdoll.GetChild(i));
            }
            ragdoll.GetChild(i).localPosition = origin.GetChild(i).localPosition;
            ragdoll.GetChild(i).localRotation = origin.GetChild(i).localRotation;
        }
    }
}
