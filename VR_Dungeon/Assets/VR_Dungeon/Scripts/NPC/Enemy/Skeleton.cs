using Scenes.AI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Skeleton : MonoBehaviour, IDamagable
{
    public GameObject[] _players;
    public GameObject _target;
    public Transform _eye;

    private Animator _animator;
    private Animation _animation;
    private NavMeshAgent _agent;
    private Coroutine _lifeRoutine;
    private AudioSource _audioSource;
    private RagdollChanger _ragdollChanger;
    private client _client;

    public EnemyState _enemyState;
    private Vector3 _targetPos;
    private float _timer = 0f;

    [SerializeField] private Stat stat;
    [SerializeField] private float sightAngle = 0.2f;
    [SerializeField] private float sightLength = 2f;
    [SerializeField] private LayerMask layerToCast;
    [SerializeField] private float attackRange = 2f;
    [SerializeField] private List<AudioClip> audioClips = new List<AudioClip>();

    public float Hp => stat.Hp;

    public float MaxHp => stat.MaxHp;

    void Start()
    {
        _players = GameObject.FindGameObjectsWithTag("Player");
        _client = GameObject.Find("Server").GetComponent<client>();

        if (_client.isHost)
            _agent = GetComponent<NavMeshAgent>();
        else
        {
            _agent = GetComponent<NavMeshAgent>();
            _agent.enabled = false;
        }

        _animator = GetComponent<Animator>();
        _animation = GetComponent<Animation>();
        _audioSource = GetComponent<AudioSource>();
        _ragdollChanger = GetComponent<RagdollChanger>();
    }

    private void OnEnable()
    {
        _timer = float.PositiveInfinity;
        _enemyState = EnemyState.Idle;
        stat.AddHp(stat.MaxHp);
        _lifeRoutine = StartCoroutine(LifeRoutine());
    }

    private void OnDisable()
    {
        if (_lifeRoutine != null) StopCoroutine(_lifeRoutine);

        _lifeRoutine = null;
    }

    private IEnumerator LifeRoutine()
    {
        while (_enemyState != EnemyState.Dead)
        {
            if (_enemyState == EnemyState.Idle) Idle();
            else if (_enemyState == EnemyState.Finding) Finding();
            else if (_enemyState == EnemyState.Chasing) Chasing();
            else if (_enemyState == EnemyState.Attacking) Attack();
            yield return null;
        }

        Death();
    }

    private void Death()
    {
        if (_client.isHost)
            _agent.SetDestination(transform.position);
        _ragdollChanger.ChangeRagdoll();
    }

    private void Attack()
    {
        if (_client.isHost)
            _agent.SetDestination(transform.position);

        if ((_target.transform.position - transform.position).magnitude <= attackRange)
        {
            if (_timer >= _animation.GetClip("attack").length)
            {
                transform.rotation = Quaternion.LookRotation((_target.transform.position - transform.position).normalized);
                ActiveAttack();

                _timer = 0f;
            }
            else
                _timer += Time.deltaTime;
        }
        else
        {
            DeactiveAttack();
            _enemyState = EnemyState.Chasing;
        }
    }


    private void Chasing()
    {
        var dir = transform.position - _target.transform.position;

        if (_client.isHost)
        {
            if (dir.magnitude < 20f)
                _agent.SetDestination(_target.transform.position);
            else
            {
                _agent.SetDestination(transform.position);
                _target = null;
                _enemyState = EnemyState.Finding;
            }
        }
        else
        {
            if (dir.magnitude >= 20f)
            {
                _target = null;
                _enemyState = EnemyState.Finding;
            }
        }

        if (dir.magnitude <= attackRange)
            _enemyState = EnemyState.Attacking;

    }

    private void Finding()
    {
        for (int i = 0; i < _players.Length; ++i)
        {
            var angle = Vector3.Dot((_players[i].transform.position - transform.position).normalized,
                transform.forward);

            if (angle > sightAngle && Physics.Raycast(_eye.transform.position,
                    (_players[i].transform.position - _eye.transform.position).normalized, out var racastHit,
                    sightLength, layerToCast) && racastHit.collider.CompareTag("Player"))
            {
                _enemyState = EnemyState.Chasing;
                _target = _players[i];
                return;
            }
        }
    }

    private void Idle()
    {
        _enemyState = EnemyState.Finding;
    }

    void IDamagable.Damage(float damageAmount)
    {
        stat.AddHp(-damageAmount);
        if (stat.Hp <= 0)
        {
            _enemyState = EnemyState.Dead;
        }
    }

    private void ActiveAttack()
    {
        _audioSource.clip = audioClips[0];
        _animator.enabled = false;
        gameObject.GetComponent<MotionAnimator>().enabled = false;
        _target.GetComponent<IDamagable>().Damage(1);
        _animation.enabled = true;
        _animation.Play("attack");
        _audioSource.Play();
    }
    private void DeactiveAttack()
    {
        _animation.Play("walk");
        _animation.enabled = false;
        _animator.enabled = true;
        _animator.Rebind();
        gameObject.GetComponent<MotionAnimator>().enabled = true;
    }
}
