using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WalkTest : MonoBehaviour
{
    private NavMeshAgent _agent;

    void Start()
    {
        _agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Ray targetPos = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(targetPos, out RaycastHit hit))
                _agent.SetDestination(hit.point);
        }
    }
}
