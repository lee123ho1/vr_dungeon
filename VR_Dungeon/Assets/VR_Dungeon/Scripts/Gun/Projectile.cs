using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scenes.AI;

public class Projectile : MonoBehaviour
{
    private Rigidbody _rigidbody;
    private Coroutine _proojectileRoutine;

    private float _speed = 50;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision other)
    {
        var damagable = other.gameObject.GetComponent<IDamagable>();
        damagable?.Damage(1);
        gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        _proojectileRoutine = StartCoroutine(BulletLifeRoutine());
        _rigidbody.velocity = Vector3.zero;
    }

    private void OnDisable()
    {
        if (_proojectileRoutine == null) return;

        StopCoroutine(_proojectileRoutine);
        _proojectileRoutine = null;
    }

    private IEnumerator BulletLifeRoutine()
    {
        yield return new WaitForSeconds(3f);
        gameObject.SetActive(false);
    }

    public void Shoot(Vector3 vec)
    {
        _rigidbody.AddForce(vec * _speed, ForceMode.Impulse);
    }
}
