using Project.Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using Valve.VR;

public class Blaster : MonoBehaviour
{
    [SerializeField] private SteamVR_Action_Boolean m_FireAction = null;
    [SerializeField] private AudioSource shotSound;
    [SerializeField] private Transform barrel;
    [SerializeField] private VisualEffect visualEffect;
    [SerializeField] private GameObject muzzelFlash;
    private SteamVR_Behaviour_Pose m_Pose = null;
    private Coroutine _muzzleFlashCoroutine;
    private bool inHand = false;
    private bool resetHand = true;

    private void Update()
    {
        if (inHand && resetHand)
        {
            m_Pose = transform.parent.GetComponentInParent<SteamVR_Behaviour_Pose>();
            resetHand = false;
        }

        if (m_Pose != null && m_FireAction.GetStateDown(m_Pose.inputSource) && inHand)
            Fire();

        if (Input.GetKeyDown(KeyCode.Space))
            Fire();
    }

    public void Fire()
    {
        shotSound.Play();
        visualEffect.Play();
        _muzzleFlashCoroutine = StartCoroutine(MuzzleFlash());
    }

    IEnumerator MuzzleFlash()
    {
        muzzelFlash.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        muzzelFlash.SetActive(false);
        StopCoroutine(_muzzleFlashCoroutine);
    }

    public void PickUp()
    {
        inHand = true;
    }

    public void PickDown()
    {
        inHand = false;
        resetHand = true;
    }
}
