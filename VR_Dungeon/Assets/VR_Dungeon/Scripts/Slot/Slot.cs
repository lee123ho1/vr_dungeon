using Project.Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class Slot : MonoBehaviour
{
    [SerializeField] private Transform _player;

    private Vector3 _slotPos;
    public int _onItem = 0;

    private void Awake()
    {
        _slotPos = transform.localPosition;
    }

    private void Update()
    {
        transform.localPosition = _player.localPosition + new Vector3(_slotPos.x, -_slotPos.y, _slotPos.z);
        if (transform.localPosition.y <= 0.25f)
            transform.localPosition = new Vector3(transform.localPosition.x, 0.25f, transform.localPosition.z);
        //transform.localEulerAngles = new Vector3(0f, _player.localEulerAngles.y, 0f);
        transform.RotateAround(_player.transform.position, Vector3.up, _player.localEulerAngles.y);
        transform.localEulerAngles = new Vector3(0f, _player.localEulerAngles.y, 0f);
    }
}
