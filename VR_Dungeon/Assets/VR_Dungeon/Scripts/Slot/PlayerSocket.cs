using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class PlayerSocket : Throwable
{
    public GameObject target;

    public Valve.VR.InteractionSystem.Hand _hand;

    private void Update()
    {
        _hand.AttachObject(target, GrabTypes.Scripted, attachmentFlags);
    }
}
