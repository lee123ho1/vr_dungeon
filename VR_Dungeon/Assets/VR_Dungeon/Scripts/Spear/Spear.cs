using Project.Manager;
using Scenes.AI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spear : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            var damagable = other.gameObject.GetComponent<IDamagable>();
            damagable?.Damage(10);
        }
    }
}
