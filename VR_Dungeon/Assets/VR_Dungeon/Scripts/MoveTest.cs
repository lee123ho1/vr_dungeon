using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTest : MonoBehaviour
{
    public float speed = 1f;
    public bool a, b, c;
    public Transform Head;

    void Update()
    {
        if (a)
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
        else if (b)
            transform.position += new Vector3(0, 0, speed * 0.1f);
        else if (c)
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(Head.position.x, 0, Head.position.z), speed * Time.deltaTime);
    }
}
