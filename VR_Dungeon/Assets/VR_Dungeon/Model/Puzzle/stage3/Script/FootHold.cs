using Project.Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FootHold : MonoBehaviour
{
    public Image image;
    public Sprite blood;
    public Sprite NonTexture;
    public bool check = false;
    public bool pass = false;
    public List<AudioClip> audioClips = new List<AudioClip>();

    private GameObject _spear;
    private Coroutine _coroutine;
    private AudioSource _audioSource;
    private Vector3 _footholdPos;
    private Vector3 _spearPos;
    private bool isRunning = false;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        _footholdPos = transform.position;
        if (!pass)
            image.sprite = blood;
        else
        {
            image.sprite = NonTexture;
            image.color = Color.clear;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            check = true;

            if (!pass && !isRunning)
            {
                isRunning = true;
                _spear = ObjectPoolManager.Instance.Spawn("spear");
                _spear.SetActive(true);
                _spear.transform.position = transform.position + new Vector3(0, 5.5f, 0);

                _coroutine = StartCoroutine(ActiveSpear(5));
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            check = false;
        }
    }

    private void Update()
    {
        if (check)
        {
            if ((transform.position - _footholdPos).magnitude < 0.2f)
                transform.Translate(Vector3.up * Time.deltaTime * -0.3f);
        }
        else
            if ((transform.position - _footholdPos).magnitude > 0.01f)
            transform.Translate(Vector3.up * Time.deltaTime * 0.3f);
    }

    private IEnumerator ActiveSpear(int num)
    {
        int count = 0;

        while (count < num)
        {
            _spear.transform.Translate(Vector3.up * Time.deltaTime * -30f);
            count++;
            yield return new WaitForFixedUpdate();
        }
        _audioSource.clip = audioClips[0];
        _audioSource.Play();

        yield return new WaitForSeconds(0.5f);
        count = 0;

        while (count < num)
        {
            _spear.transform.Translate(Vector3.up * Time.deltaTime * 30f);
            count++;
            yield return new WaitForFixedUpdate();
        }
        _audioSource.clip = audioClips[1];
        _audioSource.Play();
        _spear.SetActive(false);
        isRunning = false;
        StopCoroutine(_coroutine);
    }
}
