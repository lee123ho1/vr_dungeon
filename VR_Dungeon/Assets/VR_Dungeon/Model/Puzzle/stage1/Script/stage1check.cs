using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class stage1check : MonoBehaviour
{
    public int Count;
    public AudioClip lockAudio;
    public AudioClip unlockAudio;
    AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (Count == 5)
        {
            Count++;
        }
    }

    public void goNext()
    {
        AudioClip audio;
        if (Count == 6)
            audio = unlockAudio;
        else 
            audio = lockAudio;

        audioSource.clip = audio;
        audioSource.Play();

        if (Count == 6)
            SceneManager.LoadScene("map3");
    }
}
