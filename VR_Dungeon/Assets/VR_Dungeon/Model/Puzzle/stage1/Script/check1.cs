using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class check1 : MonoBehaviour
{
    public stage1check checker;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("puzzle") && GetComponent<FixedJoint>() == null)
        {
            Debug.Log("Enter");
            var joint = AddFixedJoint();
            joint.connectedBody = other.gameObject.GetComponentInParent<Rigidbody>();

            GameObject.Destroy(other.GetComponentInParent<Throwable>());
            GameObject.Destroy(other.GetComponentInParent<Interactable>());

            checker.Count++;
        }
    }

    FixedJoint AddFixedJoint()
    {
        FixedJoint fx = gameObject.AddComponent<FixedJoint>();
        fx.breakForce = 500f;
        return fx;
    }
}
