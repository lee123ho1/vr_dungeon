using System;
using System.Runtime.InteropServices;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Net.Sockets;
using Valve.VR;
using Valve.VR.InteractionSystem;
using UnityEngine.SceneManagement;

static class Packet
{
    public const byte CS_PACKET_LOGIN = (byte)0;
    public const byte CS_PACKET_MOVE = (byte)1;
    public const byte CS_PACKET_GRAB = (byte)2;
    public const byte CS_PACKET_CHANGE_HAND_STATE = (byte)3;
    public const byte CS_PACKET_MONSTER_MOVE = (byte)4;
    public const byte CS_PACKET_HAND_MOVE = (byte)5;
    public const byte CS_PACKET_HEAD_MOVE = (byte)6;
    public const byte CS_PACKET_SERVER_HAND_MOVE = (byte)7;
    public const byte CS_PACKET_CHANGE_MONSTER_STATE = (byte)8;
    public const byte CS_PACKET_CHANGE_SCENE = (byte)9;
    public const byte CS_PACKET_Y_POS = (byte)10;
    public const byte CS_PACKET_MAP4_CLEAR = (byte)11;
    public const byte CS_PACKET_MAP3_GEM = (byte)12;
    public const byte CS_PACKET_ENTER_TORCH = (byte)13;
    public const byte CS_PACKET_TORCH_POS = (byte)14;

    public const byte SC_PACKET_LOGIN_OK = (byte)0;
    public const byte SC_PACKET_MOVE = (byte)1;
    public const byte SC_PACKET_ENTER = (byte)2;
    public const byte SC_PACKET_GRAB = (byte)3;
    public const byte SC_PACKET_OBJECT_MOVE = (byte)4;
    public const byte SC_PACKET_CHANGE_HAND_STATE = (byte)5;
    public const byte SC_PACKET_MONSTER_MOVE = (byte)6;
    public const byte SC_PACKET_MONSTER_REMOVE = (byte)7;
    public const byte SC_PACKET_HAND_MOVE = (byte)8;
    public const byte SC_PACKET_HEAD_MOVE = (byte)9;
    public const byte SC_PACKET_CHANGE_MONSTER_STATE = (byte)10;
    public const byte SC_PACKET_MAP2_CLEAR = (byte)11;
    public const byte SC_PACKET_Y_POS = (byte)12;
    public const byte SC_PACKET_CHANGE_SCENE = (byte)13;
    public const byte SC_PACKET_GET_GEM4 = (byte)14;
    public const byte SC_PACKET_GET_GEM5 = (byte)15;
    public const byte SC_PACKET_MAP3_GEM = (byte)16;
    public const byte SC_PACKET_ENTER_TORCH = (byte)17;
    public const byte SC_PACKET_TORCH_POS = (byte)18;

    public static byte[] serialize(object obj)
    {
        char size = (char)Marshal.SizeOf(obj.GetType());
        var buffer = new byte[size];
        var gch = GCHandle.Alloc(buffer, GCHandleType.Pinned);
        var pBuffer = gch.AddrOfPinnedObject();
        Marshal.StructureToPtr(obj, pBuffer, false);
        gch.Free();

        return buffer;
    }

    public static object deSerialize(byte[] data, Type dataType)
    {
        int size = Marshal.SizeOf(dataType);

        if (size > data.Length)
            return null;

        IntPtr buffer = Marshal.AllocHGlobal(size);
        Marshal.Copy(data, 0, buffer, size);
        object retobj = Marshal.PtrToStructure(buffer, dataType);
        Marshal.FreeHGlobal(buffer);

        return retobj;
    }
}

static class Hand
{
    public const bool LEFT_HAND = true;
    public const bool RIGHT_HAND = false;
}

public enum HandState
{
    eDEFAULT = 0,
    eGUN,
    eTORCHLIGHT,
    eGEM4,
    eGEM5
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct cs_packet_login
{
    public char size;
    public char type;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct cs_packet_move
{
    public char size;
    public char type;
    public float body_pX, body_pY, body_pZ;
    public float body_rX, body_rY, body_rZ, body_rW;
    public float lHand_pX, lHand_pY, lHand_pZ;
    public float lHand_rX, lHand_rY, lHand_rZ, lHand_rW;
    public float rHand_pX, rHand_pY, rHand_pZ;
    public float rHand_rX, rHand_rY, rHand_rZ, rHand_rW;

    public void setTransform(GameObject body, GameObject lHand, GameObject rHand)
    {
        body_pX = body.transform.position.x;
        body_pY = body.transform.position.y;
        body_pZ = body.transform.position.z;

        body_rX = body.transform.rotation.x;
        body_rY = body.transform.rotation.y;
        body_rZ = body.transform.rotation.z;
        body_rW = body.transform.rotation.w;

        lHand_pX = lHand.transform.position.x;
        lHand_pY = lHand.transform.position.y;
        lHand_pZ = lHand.transform.position.z;

        lHand_rX = lHand.transform.rotation.x;
        lHand_rY = lHand.transform.rotation.y;
        lHand_rZ = lHand.transform.rotation.z;
        lHand_rW = lHand.transform.rotation.w;

        rHand_pX = rHand.transform.position.x;
        rHand_pY = rHand.transform.position.y;
        rHand_pZ = rHand.transform.position.z;

        rHand_rX = rHand.transform.rotation.x;
        rHand_rY = rHand.transform.rotation.y;
        rHand_rZ = rHand.transform.rotation.z;
        rHand_rW = rHand.transform.rotation.w;
    }
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct cs_packet_grab
{
    public char size;
    public char type;
    [MarshalAs(UnmanagedType.I1)]
    public bool hand;
    [MarshalAs(UnmanagedType.I1)]
    public bool grab;
    public float pX, pY, pZ;
    public float dirX, dirY, dirZ;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct cs_packet_change_hand_state
{
    public char size;
    public char type;
    [MarshalAs(UnmanagedType.I1)]
    public bool hand;
    public HandState state;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct cs_packet_monster_move
{
    public char size;
    public char type;
    public int id;
    public float pX, pY, pZ;
    public uint quat;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct cs_packet_hand_move
{
    public char size;
    public char type;
    public float lx, ly, lz;
    public uint lQuat;
    public float rx, ry, rz;
    public uint rQuat;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct cs_packet_head_move
{
    public char size;
    public char type;
    public float x, y, z;
    public uint quat;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct cs_packet_server_hand_move
{
    public char size;
    public char type;
    public float lHand_pX, lHand_pY, lHand_pZ;
    public uint lHandQuat;
    public float rHand_pX, rHand_pY, rHand_pZ;
    public uint rHandQuat;

    public void setTransform(GameObject lHand, GameObject rHand)
    {
        lHand_pX = lHand.transform.position.x;
        lHand_pY = lHand.transform.position.y;
        lHand_pZ = lHand.transform.position.z;

        rHand_pX = rHand.transform.position.x;
        rHand_pY = rHand.transform.position.y;
        rHand_pZ = rHand.transform.position.z;
    }
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct cs_packet_change_monster_state
{
    public char size;
    public char type;
    public int monster_id;
    public Scenes.AI.EnemyState state;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct cs_packet_change_scene
{
    public char size;
    public char type;
    public int scene_id;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct cs_packet_y_pos
{
    public char size;
    public char type;
    public float y;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct cs_packet_map4_clear
{
    public char size;
    public char type;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct cs_packet_map3_gem
{
    public char size;
    public char type;
    [MarshalAs(UnmanagedType.I1)]
    public bool side;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct cs_packet_enter_torch
{
    public char size;
    public char type;
    [MarshalAs(UnmanagedType.I1)]
    public bool enter;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct cs_packet_torch_pos
{
    public char size;
    public char type;
    public float x, y, z;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct sc_packet_login_ok
{
    public char size;
    public char type;
    public int id;
    public float pX, pY, pZ;
    public float rX, rY, rZ, rW;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct sc_packet_move
{
    public char size;
    public char type;
    public int id;
    public float body_pX, body_pY, body_pZ;
    public float body_rX, body_rY, body_rZ, body_rW;
    public float lHand_pX, lHand_pY, lHand_pZ;
    public float lHand_rX, lHand_rY, lHand_rZ, lHand_rW;
    public float rHand_pX, rHand_pY, rHand_pZ;
    public float rHand_rX, rHand_rY, rHand_rZ, rHand_rW;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct sc_packet_enter
{
    public char size;
    public char type;
    public int id;
    public float pX, pY, pZ;
    public float rX, rY, rZ, rW;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct sc_packet_grab
{
    public char size;
    public char type;
    public int id;
    [MarshalAs(UnmanagedType.I1)]
    public bool hand;
    [MarshalAs(UnmanagedType.I1)]
    public bool grab;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct sc_packet_object_move
{
    public char size;
    public char type;
    public int id;
    public float pX, pY, pZ;
    public uint quat;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct sc_packet_change_hand_state
{
    public char size;
    public char type;
    public int id;
    [MarshalAs(UnmanagedType.I1)]
    public bool hand;
    public HandState state;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct sc_packet_monster_move
{
    public char size;
    public char type;
    public int id;
    public float pX, pY, pZ;
    public uint quat;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct sc_packet_monster_remove
{
    public char size;
    public char type;
    public int id;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct sc_packet_hand_move
{
    public char size;
    public char type;
    public float lHand_pX, lHand_pY, lHand_pZ;
    public uint lHandQuat;
    public float rHand_pX, rHand_pY, rHand_pZ;
    public uint rHandQuat;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct sc_packet_head_move
{
    public char size;
    public char type;
    public float x, y, z;
    public uint quat;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct sc_packet_change_monster_state
{
    public char size;
    public char type;
    public int monster_id;
    public Scenes.AI.EnemyState state;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct sc_packet_map2_clear
{
    public char size;
    public char type;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct sc_packet_y_pos
{
    public char size;
    public char type;
    public float y;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct sc_packet_change_scene
{
    public char size;
    public char type;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct sc_packet_get_gem4
{
    public char size;
    public char type;
    public int owner_id;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct sc_packet_get_gem5
{
    public char size;
    public char type;
    public int owner_id;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct sc_packet_map3_gem
{
    public char size;
    public char type;
    [MarshalAs(UnmanagedType.I1)]
    public bool side;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct sc_packet_enter_torch
{
    public char size;
    public char type;
    [MarshalAs(UnmanagedType.I1)]
    public bool enter;
}

[Serializable]
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct sc_packet_torch_pos
{
    public char size;
    public char type;
    public float x, y, z;
}

public enum MonsterType
{
    eScorpion,
    eSkeleton
}

[Serializable]
public struct Monster
{
    public GameObject monsterObject;
    public MonsterType monsterType;
    public Scenes.AI.EnemyState lastState;
    public System.Object script;
}

public class Compressed_quaternion
{
    const int bits = 10;
    public static uint load(Quaternion quaternion)
    {
        const float minimum = -1.0f / 1.414214f;       // 1.0f / sqrt(2)
        const float maximum = +1.0f / 1.414214f;

        const float scale = (float)((1 << bits) - 1);

        float x = quaternion.x;
        float y = quaternion.y;
        float z = quaternion.z;
        float w = quaternion.w;

        float abs_x = Math.Abs(x);
        float abs_y = Math.Abs(y);
        float abs_z = Math.Abs(z);
        float abs_w = Math.Abs(w);

        float largest_value = abs_x;

        uint largest = 0;

        if (abs_y > largest_value)
        {
            largest = 1;
            largest_value = abs_y;
        }

        if (abs_z > largest_value)
        {
            largest = 2;
            largest_value = abs_z;
        }

        if (abs_w > largest_value)
        {
            largest = 3;
            largest_value = abs_w;
        }

        float a = 0;
        float b = 0;
        float c = 0;

        switch (largest)
        {
            case 0:
                if (x >= 0)
                {
                    a = y;
                    b = z;
                    c = w;
                }
                else
                {
                    a = -y;
                    b = -z;
                    c = -w;
                }
                break;

            case 1:
                if (y >= 0)
                {
                    a = x;
                    b = z;
                    c = w;
                }
                else
                {
                    a = -x;
                    b = -z;
                    c = -w;
                }
                break;

            case 2:
                if (z >= 0)
                {
                    a = x;
                    b = y;
                    c = w;
                }
                else
                {
                    a = -x;
                    b = -y;
                    c = -w;
                }
                break;

            case 3:
                if (w >= 0)
                {
                    a = x;
                    b = y;
                    c = z;
                }
                else
                {
                    a = -x;
                    b = -y;
                    c = -z;
                }
                break;
        }

        float normal_a = (a - minimum) / (maximum - minimum);
        float normal_b = (b - minimum) / (maximum - minimum);
        float normal_c = (c - minimum) / (maximum - minimum);

        uint integer_a = (uint)Math.Floor(normal_a * scale + 0.5f);
        uint integer_b = (uint)Math.Floor(normal_b * scale + 0.5f);
        uint integer_c = (uint)Math.Floor(normal_c * scale + 0.5f);

        return (largest) | (integer_a << 2) | (integer_b << 12) | (integer_c << 22);
    }

    public static Quaternion save(uint quat)
    {
        const float minimum = -1.0f / 1.414214f;       // 1.0f / sqrt(2)
        const float maximum = +1.0f / 1.414214f;

        const float scale = (float)((1 << bits) - 1);

        const float inverse_scale = 1.0f / scale;

        uint largest = (quat & 0b_0000_0000_0000_0000_0000_0000_0000_0011);
        uint integer_a = (quat & 0b_0000_0000_0000_0000_0000_1111_1111_1100) >> 2;
        uint integer_b = (quat & 0b_0000_0000_0011_1111_1111_0000_0000_0000) >> 12;
        uint integer_c = (quat & 0b_1111_1111_1100_0000_0000_0000_0000_0000) >> 22;

        float a = integer_a * inverse_scale * (maximum - minimum) + minimum;
        float b = integer_b * inverse_scale * (maximum - minimum) + minimum;
        float c = integer_c * inverse_scale * (maximum - minimum) + minimum;

        float x = 0.0f;
        float y = 0.0f;
        float z = 0.0f;
        float w = 0.0f;

        switch (largest)
        {
            case 0:
                {
                    x = (float)Math.Sqrt(1 - a * a - b * b - c * c);
                    y = a;
                    z = b;
                    w = c;
                }
                break;

            case 1:
                {
                    x = a;
                    y = (float)Math.Sqrt(1 - a * a - b * b - c * c);
                    z = b;
                    w = c;
                }
                break;

            case 2:
                {
                    x = a;
                    y = b;
                    z = (float)Math.Sqrt(1 - a * a - b * b - c * c);
                    w = c;
                }
                break;

            case 3:
                {
                    x = a;
                    y = b;
                    z = c;
                    w = (float)Math.Sqrt(1 - a * a - b * b - c * c);
                }
                break;
        }

        // IMPORTANT: We must normalize the quaternion here because it will have slight drift otherwise due to being quantized

        float norm = x * x + y * y + z * z + w * w;

        if (norm > 0.000001f)
        {
            var quaternion = new Quaternion(x, y, z, w);
            float length = (float)Math.Sqrt(norm);
            quaternion.x /= length;
            quaternion.y /= length;
            quaternion.z /= length;
            quaternion.w /= length;
            return quaternion;
        }
        else
        {
            return new Quaternion(0, 0, 0, 1);
        }
    }
}

public class client : MonoBehaviour
{
    private Socket clientSocket = null;
    private int in_packet_size = 0;
    private int saved_packet_size = 0;
    private byte[] packet_buffer = new byte[4096];
    private byte[] packet = new byte[4096];
    public string ip = "127.0.0.1";
    public const int port = 9000;
    private bool myConnect = false;
    private bool otherConnect = false;
    public GameObject textbox;

    public GameObject objectContainer;
    private GameObject[] objects;

    public GameObject monsterContainer;
    public Monster[] monsters;

    public bool isVR;
    public bool isHost;
    private GameObject[] players;

    private GameObject myBody, myLHand, myRHand;
    private GameObject ol, or;
    private GameObject vrHead;
    private Valve.VR.InteractionSystem.Hand lHandScript, rHandScript;
    private bool objInLHand, objInRHand;

    private GameObject otherBody, otherLHand, otherRHand;
    private GameObject otherHead;
    private GameObject otherLStuff, otherRStuff;
    private HandState otherLHandState, otherRHandState;
    private Vector3 lastPosition;

    public GameObject gun;
    public Transform barrel;
    public GameObject torchLight;
    public GameObject jewel4Parent;
    private GameObject jewel4;
    public GameObject jewel5Parent;
    private GameObject jewel5;

    private Land finalMapLand;

    private int my_id;
    public int MyId => my_id;
    private int other_id;

    TaskExecutorScript taskExecutorScript;

    public void send(byte[] sendPacket)
    {
        clientSocket.Send(sendPacket, 0, (int)sendPacket[0], SocketFlags.None);
    }

    public void change_scene()
    {
        players[other_id] = GameObject.Find("Other");
        Transform other = players[other_id].transform.Find("Rig 1");
        otherBody = other.parent.gameObject;
        otherLHand = other.GetChild(0).transform.GetChild(0).gameObject;
        otherRHand = other.GetChild(1).transform.GetChild(0).gameObject;
        otherHead = players[other_id].transform.Find("headData").gameObject;
        otherLStuff = GameObject.FindGameObjectWithTag("OtherLHand");
        otherRStuff = GameObject.FindGameObjectWithTag("OtherRHand");
        objInLHand = objInRHand = false;

        objectContainer = GameObject.Find("ObjectContainer");
        int nbObject = objectContainer.transform.childCount;
        objects = new GameObject[nbObject];
        for (int i = 0; i < nbObject; ++i)
        {
            objects[i] = objectContainer.transform.GetChild(i).gameObject;
        }

        monsterContainer = GameObject.Find("MonsterContainer");
        monsters = monsterContainer.GetComponent<MonsterContainer>().monsters;

        for (int i = 0; i < monsters.Length; ++i)
        {
            switch (monsters[i].monsterType)
            {
                case MonsterType.eScorpion:
                    {
                        monsters[i].script = monsters[i].monsterObject.GetComponent<Scorpion>();
                        break;
                    }
                case MonsterType.eSkeleton:
                    {
                        monsters[i].script = monsters[i].monsterObject.GetComponent<Skeleton>();
                        break;
                    }
            }
        }

        cs_packet_change_scene p = new cs_packet_change_scene();
        p.size = (char)Marshal.SizeOf(typeof(cs_packet_change_scene));
        p.type = (char)Packet.CS_PACKET_CHANGE_SCENE;
        switch (SceneManager.GetActiveScene().name)
        {
            case "map2":
                {
                    p.scene_id = 2;
                    break;
                }
            case "map3":
                {
                    p.scene_id = 3;
                    break;
                }
            case "map4":
                {
                    p.scene_id = 4;
                    break;
                }
            case "map5":
                {
                    p.scene_id = 5;
                    break;
                }
            case "Final_Stage":
                {
                    p.scene_id = 6;
                    break;
                }
        }

        send(Packet.serialize(p));
    }

    void processPacket(byte[] packet)
    {
        switch (packet[1])
        {
            case Packet.SC_PACKET_LOGIN_OK:
                {
                    //Debug.Log("login");
                    sc_packet_login_ok p = new sc_packet_login_ok();
                    p = (sc_packet_login_ok)Packet.deSerialize(packet, p.GetType());

                    my_id = p.id;
                    other_id = 1 - my_id;

                    taskExecutorScript.ScheduleTask(new Task(delegate
                    {
                        players[my_id] = GameObject.Find("Player");
                        Transform player = players[my_id].transform.GetChild(0).gameObject.transform;
                        myBody = player.GetChild(0).gameObject;
                        if (isVR)
                        {
                            lHandScript = player.GetChild(1).GetComponent<Valve.VR.InteractionSystem.Hand>();
                            rHandScript = player.GetChild(2).GetComponent<Valve.VR.InteractionSystem.Hand>();
                        }
                        myLHand = player.GetChild(1).Find("LeftHandData").gameObject;
                        ol = myLHand.transform.GetChild(0).gameObject;
                        myRHand = player.GetChild(2).Find("RightHandData").gameObject;
                        or = myRHand.transform.GetChild(0).gameObject;
                        vrHead = player.Find("VRCamera").gameObject;
                        myConnect = true;

                        players[other_id] = GameObject.Find("Other");
                        Transform other = players[other_id].transform.Find("Rig 1");
                        otherBody = other.parent.gameObject;
                        otherLHand = other.GetChild(0).transform.GetChild(0).gameObject;
                        otherRHand = other.GetChild(1).transform.GetChild(0).gameObject;
                        otherHead = players[other_id].transform.Find("headData").gameObject;
                    }));

                    break;
                }
            case Packet.SC_PACKET_MOVE:
                {
                    //Debug.Log("Receive Move Packet");
                    sc_packet_move p = new sc_packet_move();
                    p = (sc_packet_move)Packet.deSerialize(packet, p.GetType());
                    Vector3 bodyPos = new Vector3(p.body_pX, p.body_pY, p.body_pZ);
                    Quaternion bodyQuat = new Quaternion(p.body_rX, p.body_rY, p.body_rZ, p.body_rW);
                    Vector3 lHandPos = new Vector3(p.lHand_pX, p.lHand_pY, p.lHand_pZ);
                    Quaternion lHandQuat = new Quaternion(p.lHand_rX, p.lHand_rY, p.lHand_rZ, p.lHand_rW);
                    Vector3 rHandPos = new Vector3(p.rHand_pX, p.rHand_pY, p.rHand_pZ);
                    Quaternion rHandQuat = new Quaternion(p.rHand_rX, p.rHand_rY, p.rHand_rZ, p.rHand_rW);
                    //Debug.Log("move " + p.id);

                    taskExecutorScript.ScheduleTask(new Task(delegate
                    {
                        otherBody.transform.position = bodyPos;
                        otherBody.transform.rotation = bodyQuat;
                        otherLHand.transform.position = lHandPos;
                        otherLHand.transform.rotation = lHandQuat;
                        otherRHand.transform.position = rHandPos;
                        otherRHand.transform.rotation = rHandQuat;
                    }));

                    break;
                }
            case Packet.SC_PACKET_ENTER:
                {
                    Debug.Log("Receive Enter Packet");
                    //sc_packet_enter p = new sc_packet_enter();
                    //p = (sc_packet_enter)Packet.deSerialize(packet, p.GetType());
                    //Vector3 pos = new Vector3(p.pX, p.pY, p.pZ);
                    //Quaternion quat = new Quaternion(p.rX, p.rY, p.rZ, p.rW);

                    taskExecutorScript.ScheduleTask(new Task(delegate
                    {
                        otherConnect = true;
                    }));
                    break;
                }
            case Packet.SC_PACKET_GRAB:
                {
                    sc_packet_grab p = new sc_packet_grab();
                    p = (sc_packet_grab)Packet.deSerialize(packet, p.GetType());

                    taskExecutorScript.ScheduleTask(new Task(delegate
                    {
                        if (p.grab)
                        {
                            switch (p.hand)
                            {
                                case Hand.LEFT_HAND:
                                    {
                                        otherLStuff.transform.Find("Gun").Find("Gun (1)").GetComponent<Blaster>().Fire();
                                        break;
                                    }
                                case Hand.RIGHT_HAND:
                                    {
                                        otherRStuff.transform.Find("Gun").Find("Gun (1)").GetComponent<Blaster>().Fire();
                                        break;
                                    }
                            }
                        }
                    }));
                    break;
                }
            case Packet.SC_PACKET_OBJECT_MOVE:
                {
                    sc_packet_object_move p = new sc_packet_object_move();
                    p = (sc_packet_object_move)Packet.deSerialize(packet, p.GetType());
                    Vector3 pos = new Vector3(p.pX, p.pY, p.pZ);
                    Quaternion quat = Compressed_quaternion.save(p.quat);

                    taskExecutorScript.ScheduleTask(new Task(delegate
                    {
                        objects[p.id].transform.position = pos;
                        objects[p.id].transform.rotation = quat;
                    }));
                    break;
                }
            case Packet.SC_PACKET_CHANGE_HAND_STATE:
                {
                    sc_packet_change_hand_state p = new sc_packet_change_hand_state();
                    p = (sc_packet_change_hand_state)Packet.deSerialize(packet, p.GetType());

                    taskExecutorScript.ScheduleTask(new Task(delegate
                    {
                        // other의 hand의 상태를 바꿔주는 코드
                        if (p.hand == Hand.LEFT_HAND)
                        {
                            switch (p.state)
                            {
                                case HandState.eDEFAULT:
                                    {
                                        switch (otherLHandState)
                                        {
                                            case HandState.eGUN:
                                                {
                                                    otherLStuff.transform.Find("Gun").gameObject.SetActive(false);
                                                    break;
                                                }
                                            case HandState.eTORCHLIGHT:
                                                {
                                                    otherLStuff.transform.Find("OldTorch").gameObject.SetActive(false);
                                                    break;
                                                }
                                            case HandState.eGEM4:
                                                {
                                                    otherLStuff.transform.Find("Jewel4").gameObject.SetActive(false);
                                                    break;
                                                }
                                            case HandState.eGEM5:
                                                {
                                                    otherLStuff.transform.Find("Jewel5").gameObject.SetActive(false);
                                                    break;
                                                }
                                        }
                                        break;
                                    }
                                case HandState.eGUN:
                                    {
                                        switch (otherLHandState)
                                        {
                                            case HandState.eDEFAULT:
                                                {
                                                    otherLStuff.transform.Find("Gun").gameObject.SetActive(true);
                                                    break;
                                                }
                                        }
                                        break;
                                    }
                                case HandState.eTORCHLIGHT:
                                    {
                                        switch (otherLHandState)
                                        {
                                            case HandState.eDEFAULT:
                                                {
                                                    otherLStuff.transform.Find("OldTorch").gameObject.SetActive(true);
                                                    break;
                                                }
                                        }
                                        break;
                                    }
                                case HandState.eGEM4:
                                    {
                                        switch (otherLHandState)
                                        {
                                            case HandState.eDEFAULT:
                                                {
                                                    otherLStuff.transform.Find("Jewel4").gameObject.SetActive(true);
                                                    break;
                                                }
                                        }
                                        break;
                                    }
                                case HandState.eGEM5:
                                    {
                                        switch (otherLHandState)
                                        {
                                            case HandState.eDEFAULT:
                                                {
                                                    otherLStuff.transform.Find("Jewel5").gameObject.SetActive(true);
                                                    break;
                                                }
                                        }
                                        break;
                                    }
                            }
                            otherLHandState = p.state;
                        }
                        else
                        {
                            switch (p.state)
                            {
                                case HandState.eDEFAULT:
                                    {
                                        switch (otherRHandState)
                                        {
                                            case HandState.eGUN:
                                                {
                                                    otherRStuff.transform.Find("Gun").gameObject.SetActive(false);
                                                    break;
                                                }
                                            case HandState.eTORCHLIGHT:
                                                {
                                                    otherRStuff.transform.Find("OldTorch").gameObject.SetActive(false);
                                                    break;
                                                }
                                            case HandState.eGEM4:
                                                {
                                                    otherRStuff.transform.Find("Jewel4").gameObject.SetActive(false);
                                                    break;
                                                }
                                            case HandState.eGEM5:
                                                {
                                                    otherRStuff.transform.Find("Jewel5").gameObject.SetActive(false);
                                                    break;
                                                }
                                        }
                                        break;
                                    }
                                case HandState.eGUN:
                                    {
                                        switch (otherRHandState)
                                        {
                                            case HandState.eDEFAULT:
                                                {
                                                    otherRStuff.transform.Find("Gun").gameObject.SetActive(true);
                                                    break;
                                                }
                                        }
                                        break;
                                    }
                                case HandState.eTORCHLIGHT:
                                    {
                                        switch (otherRHandState)
                                        {
                                            case HandState.eDEFAULT:
                                                {
                                                    otherRStuff.transform.Find("OldTorch").gameObject.SetActive(true);
                                                    break;
                                                }
                                        }
                                        break;
                                    }
                                case HandState.eGEM4:
                                    {
                                        switch (otherRHandState)
                                        {
                                            case HandState.eDEFAULT:
                                                {
                                                    otherRStuff.transform.Find("Jewel4").gameObject.SetActive(true);
                                                    break;
                                                }
                                        }
                                        break;
                                    }
                                case HandState.eGEM5:
                                    {
                                        switch (otherRHandState)
                                        {
                                            case HandState.eDEFAULT:
                                                {
                                                    otherRStuff.transform.Find("Jewel5").gameObject.SetActive(true);
                                                    break;
                                                }
                                        }
                                        break;
                                    }
                            }
                            otherRHandState = p.state;
                        }
                    }));
                    break;
                }
            case Packet.SC_PACKET_MONSTER_MOVE:
                {
                    sc_packet_monster_move p = new sc_packet_monster_move();
                    p = (sc_packet_monster_move)Packet.deSerialize(packet, p.GetType());

                    Vector3 pos = new Vector3(p.pX, p.pY, p.pZ);
                    Quaternion quat = Compressed_quaternion.save(p.quat);

                    taskExecutorScript.ScheduleTask(new Task(delegate
                    {
                        monsters[p.id].monsterObject.transform.position = pos;
                        monsters[p.id].monsterObject.transform.rotation = quat;
                    }));
                    break;
                }
            case Packet.SC_PACKET_MONSTER_REMOVE:
                {
                    sc_packet_monster_remove p = new sc_packet_monster_remove();
                    p = (sc_packet_monster_remove)Packet.deSerialize(packet, p.GetType());

                    taskExecutorScript.ScheduleTask(new Task(delegate
                    {
                        switch (monsters[p.id].monsterType)
                        {
                            case MonsterType.eScorpion:
                                {
                                    Scorpion scorpion = (Scorpion)monsters[p.id].script;
                                    scorpion._enemyState = Scenes.AI.EnemyState.Dead;
                                    break;
                                }
                            case MonsterType.eSkeleton:
                                {
                                    Skeleton skeleton = (Skeleton)monsters[p.id].script;
                                    skeleton._enemyState = Scenes.AI.EnemyState.Dead;
                                    break;
                                }
                        }
                    }));
                    break;
                }
            case Packet.SC_PACKET_HAND_MOVE:
                {
                    sc_packet_hand_move p = new sc_packet_hand_move();
                    p = (sc_packet_hand_move)Packet.deSerialize(packet, p.GetType());

                    Vector3 lHandPos = new Vector3(p.lHand_pX, p.lHand_pY, p.lHand_pZ);
                    Quaternion lHandQuat = Compressed_quaternion.save(p.lHandQuat);
                    Vector3 rHandPos = new Vector3(p.rHand_pX, p.rHand_pY, p.rHand_pZ);
                    Quaternion rHandQuat = Compressed_quaternion.save(p.rHandQuat);
                    //Debug.Log("move " + p.id);

                    taskExecutorScript.ScheduleTask(new Task(delegate
                    {
                        otherLHand.transform.position = lHandPos;
                        otherLHand.transform.rotation = lHandQuat;
                        otherRHand.transform.position = rHandPos;
                        otherRHand.transform.rotation = rHandQuat;
                    }));

                    break;
                }
            case Packet.SC_PACKET_HEAD_MOVE:
                {
                    sc_packet_head_move p = new sc_packet_head_move();
                    p = (sc_packet_head_move)Packet.deSerialize(packet, p.GetType());

                    Vector3 pos = new Vector3(p.x, p.y, p.z);
                    Quaternion quat = Compressed_quaternion.save(p.quat);

                    taskExecutorScript.ScheduleTask(new Task(delegate
                    {
                        otherBody.transform.position = pos;
                        otherHead.transform.localRotation = quat;
                    }));

                    break;
                }
            case Packet.SC_PACKET_CHANGE_MONSTER_STATE:
                {
                    sc_packet_change_monster_state p = new sc_packet_change_monster_state();
                    p = (sc_packet_change_monster_state)Packet.deSerialize(packet, p.GetType());

                    taskExecutorScript.ScheduleTask(new Task(delegate
                    {

                    }));

                    break;
                }
            case Packet.SC_PACKET_MAP2_CLEAR:
                {
                    taskExecutorScript.ScheduleTask(new Task(delegate
                    {
                        GameObject.FindGameObjectWithTag("Stage1Checker").GetComponent<stage1check>().Count = 5;
                    }));
                    break;
                }
            case Packet.SC_PACKET_Y_POS:
                {
                    sc_packet_y_pos p = new sc_packet_y_pos();
                    p = (sc_packet_y_pos)Packet.deSerialize(packet, p.GetType());
                    taskExecutorScript.ScheduleTask(new Task(delegate
                    {
                        Vector3 pos = otherBody.transform.position;
                        pos.y = p.y;
                        otherBody.transform.position = pos;
                    }));
                    break;
                }
            case Packet.SC_PACKET_CHANGE_SCENE:
                {
                    sc_packet_change_scene p = new sc_packet_change_scene();
                    p = (sc_packet_change_scene)Packet.deSerialize(packet, p.GetType());
                    taskExecutorScript.ScheduleTask(new Task(delegate
                    {
                        Vector3 pos = new Vector3(10000, 10000, 10000);
                        players[other_id].transform.position = pos;
                    }));
                    break;
                }
            case Packet.SC_PACKET_GET_GEM4:
                {
                    sc_packet_get_gem4 p = new sc_packet_get_gem4();
                    p = (sc_packet_get_gem4)Packet.deSerialize(packet, p.GetType());
                    //Debug.Log(p.owner_id + ", " + my_id);
                    taskExecutorScript.ScheduleTask(new Task(delegate
                    {
                        GameObject.FindGameObjectWithTag("ServerGem").SetActive(false);
                        if (p.owner_id == my_id)
                        {
                            jewel4Parent.gameObject.SetActive(true);
                        }
                    }));
                    break;
                }
            case Packet.SC_PACKET_GET_GEM5:
                {
                    sc_packet_get_gem5 p = new sc_packet_get_gem5();
                    p = (sc_packet_get_gem5)Packet.deSerialize(packet, p.GetType());

                    taskExecutorScript.ScheduleTask(new Task(delegate
                    {
                        GameObject.FindGameObjectWithTag("ServerGem").SetActive(false);
                        if (p.owner_id == my_id)
                        {
                            jewel5Parent.gameObject.SetActive(true);
                        }
                    }));
                    break;
                }
            case Packet.SC_PACKET_MAP3_GEM:
                {
                    sc_packet_map3_gem p = new sc_packet_map3_gem();
                    p = (sc_packet_map3_gem)Packet.deSerialize(packet, p.GetType());

                    taskExecutorScript.ScheduleTask(new Task(delegate
                    {
                        if (p.side)
                        {
                            GameObject gameObject = GameObject.FindGameObjectWithTag("Jewel");
                            gameObject.transform.GetChild(0).gameObject.SetActive(true);
                        }
                        else
                        {
                            GameObject gameObject = GameObject.FindGameObjectWithTag("Jewel");
                            gameObject.transform.GetChild(1).gameObject.SetActive(true);
                        }
                    }));
                    break;
                }
            case Packet.SC_PACKET_ENTER_TORCH:
                {
                    sc_packet_enter_torch p = new sc_packet_enter_torch();
                    p = (sc_packet_enter_torch)Packet.deSerialize(packet, p.GetType());

                    taskExecutorScript.ScheduleTask(new Task(delegate
                    {
                        finalMapLand = GameObject.FindGameObjectWithTag("Land").GetComponent<Land>();
                        finalMapLand._isLight = true;
                    }));
                    break;
                }
            case Packet.SC_PACKET_TORCH_POS:
                {
                    sc_packet_torch_pos p = new sc_packet_torch_pos();
                    p = (sc_packet_torch_pos)Packet.deSerialize(packet, p.GetType());

                    Vector3 pos = new Vector3(p.x, p.y, p.z);

                    taskExecutorScript.ScheduleTask(new Task(delegate
                    {
                        finalMapLand.sphere.position = pos;
                    }));
                    break;
                }
            default:
                {
                    Debug.Log("Error: No Packet Type");
                    break;
                }
        }
    }

    void receive(IAsyncResult result)
    {
        Socket s = (Socket)result.AsyncState;
        int io_byte = clientSocket.EndReceive(result);
        int packetIdx = 0;
        //Debug.Log(io_byte);
        if (io_byte > 0)
        {
            while (io_byte != 0)
            {
                if (in_packet_size == 0) in_packet_size = (int)packet[packetIdx];
                if (io_byte + saved_packet_size >= in_packet_size)
                {
                    Buffer.BlockCopy(packet, packetIdx, packet_buffer, saved_packet_size, in_packet_size - saved_packet_size);
                    processPacket(packet_buffer);
                    packetIdx += in_packet_size - saved_packet_size;
                    io_byte -= in_packet_size - saved_packet_size;
                    in_packet_size = 0;
                    saved_packet_size = 0;
                }
                else
                {
                    Buffer.BlockCopy(packet, packetIdx, packet_buffer, saved_packet_size, io_byte);
                    saved_packet_size += io_byte;
                    io_byte = 0;
                }
            }
            s.BeginReceive(packet, 0, 4096, SocketFlags.None, new AsyncCallback(receive), s);
        }
        else
        {
            s.Close();
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        isHost = IP.instanceIP.isHost;

        jewel4 = jewel4Parent.transform.GetChild(0).gameObject;
        jewel5 = jewel5Parent.transform.GetChild(0).gameObject;

        players = new GameObject[2];

        otherLStuff = GameObject.FindGameObjectWithTag("OtherLHand");
        otherRStuff = GameObject.FindGameObjectWithTag("OtherRHand");
        otherLHandState = HandState.eDEFAULT;
        otherRHandState = HandState.eDEFAULT;

        objInLHand = false;
        objInRHand = false;

        objectContainer = GameObject.Find("ObjectContainer");
        int nbObject = objectContainer.transform.childCount;
        objects = new GameObject[nbObject];
        for (int i = 0; i < nbObject; ++i)
        {
            objects[i] = objectContainer.transform.GetChild(i).gameObject;
        }

        monsterContainer = GameObject.Find("MonsterContainer");
        monsters = monsterContainer.GetComponent<MonsterContainer>().monsters;

        for (int i = 0; i < monsters.Length; ++i)
        {
            switch (monsters[i].monsterType)
            {
                case MonsterType.eScorpion:
                    {
                        monsters[i].script = monsters[i].monsterObject.GetComponent<Scorpion>();
                        break;
                    }
                case MonsterType.eSkeleton:
                    {
                        monsters[i].script = monsters[i].monsterObject.GetComponent<Skeleton>();
                        break;
                    }
            }
        }

        taskExecutorScript = gameObject.GetComponent<TaskExecutorScript>();

        clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(new IPEndPoint(IPAddress.Parse(IP.instanceIP.ip), port));
        clientSocket.BeginReceive(packet, 0, 4096, SocketFlags.None, new AsyncCallback(receive), clientSocket);

        cs_packet_login p = new cs_packet_login();
        p.size = (char)Marshal.SizeOf(typeof(cs_packet_login));
        p.type = (char)Packet.CS_PACKET_LOGIN;

        send(Packet.serialize(p));

        lastPosition = new Vector3(0, 0, 0);

        cs_packet_change_scene sp = new cs_packet_change_scene();
        sp.size = (char)Marshal.SizeOf(typeof(cs_packet_change_scene));
        sp.type = (char)Packet.CS_PACKET_CHANGE_SCENE;
        sp.scene_id = 2;

        send(Packet.serialize(sp));
    }

    // Update is called once per frame
    void Update()
    {
        if (myConnect)
        {
            cs_packet_server_hand_move p = new cs_packet_server_hand_move();

            p.size = (char)Marshal.SizeOf(typeof(cs_packet_server_hand_move));
            p.type = (char)Packet.CS_PACKET_SERVER_HAND_MOVE;
            p.setTransform(myLHand, myRHand);
            p.lHandQuat = Compressed_quaternion.load(myLHand.transform.rotation);
            p.rHandQuat = Compressed_quaternion.load(myRHand.transform.rotation);

            send(Packet.serialize(p));

            cs_packet_hand_move packet_Hand_Move = new cs_packet_hand_move();
            packet_Hand_Move.size = (char)Marshal.SizeOf(typeof(cs_packet_hand_move));
            packet_Hand_Move.type = (char)Packet.CS_PACKET_HAND_MOVE;

            packet_Hand_Move.lx = ol.transform.position.x;
            packet_Hand_Move.ly = ol.transform.position.y;
            packet_Hand_Move.lz = ol.transform.position.z;
            packet_Hand_Move.lQuat = Compressed_quaternion.load(ol.transform.rotation);

            packet_Hand_Move.rx = or.transform.position.x;
            packet_Hand_Move.ry = or.transform.position.y;
            packet_Hand_Move.rz = or.transform.position.z;
            packet_Hand_Move.rQuat = Compressed_quaternion.load(or.transform.rotation);

            send(Packet.serialize(packet_Hand_Move));

            cs_packet_head_move packet_Head_Move = new cs_packet_head_move();
            packet_Head_Move.size = (char)Marshal.SizeOf(typeof(cs_packet_head_move));
            packet_Head_Move.type = (char)Packet.CS_PACKET_HEAD_MOVE;

            packet_Head_Move.x = vrHead.transform.position.x;
            packet_Head_Move.y = players[my_id].transform.position.y;
            packet_Head_Move.z = vrHead.transform.position.z;
            packet_Head_Move.quat = Compressed_quaternion.load(vrHead.transform.rotation);

            send(Packet.serialize(packet_Head_Move));

            if (isVR)
            {
                //Debug.Log("Left Hand Pinch Action Changed");
                if (lHandScript.grabPinchAction.GetStateDown(SteamVR_Input_Sources.LeftHand))
                {
                    cs_packet_grab packet = new cs_packet_grab();

                    packet.size = (char)Marshal.SizeOf(typeof(cs_packet_grab));
                    packet.type = (char)Packet.CS_PACKET_GRAB;
                    packet.hand = Hand.LEFT_HAND;
                    packet.grab = true;
                    packet.pX = barrel.position.x;
                    packet.pY = barrel.position.y;
                    packet.pZ = barrel.position.z;
                    packet.dirX = barrel.right.x;
                    packet.dirY = barrel.right.y;
                    packet.dirZ = barrel.right.z;

                    send(Packet.serialize(packet));
                }
                else if (lHandScript.grabPinchAction.GetStateUp(SteamVR_Input_Sources.LeftHand))
                {
                    cs_packet_grab packet = new cs_packet_grab();

                    packet.size = (char)Marshal.SizeOf(typeof(cs_packet_grab));
                    packet.type = (char)Packet.CS_PACKET_GRAB;
                    packet.hand = Hand.LEFT_HAND;
                    packet.grab = false;
                    packet.pX = 0;
                    packet.pY = 0;
                    packet.pZ = 0;
                    packet.dirX = 0;
                    packet.dirY = 0;
                    packet.dirZ = 0;

                    send(Packet.serialize(packet));
                }
                if (rHandScript.grabPinchAction.GetStateDown(SteamVR_Input_Sources.RightHand))
                {
                    cs_packet_grab packet = new cs_packet_grab();

                    packet.size = (char)Marshal.SizeOf(typeof(cs_packet_grab));
                    packet.type = (char)Packet.CS_PACKET_GRAB;
                    packet.hand = Hand.RIGHT_HAND;
                    packet.grab = true;
                    packet.pX = barrel.position.x;
                    packet.pY = barrel.position.y;
                    packet.pZ = barrel.position.z;
                    packet.dirX = barrel.right.x;
                    packet.dirY = barrel.right.y;
                    packet.dirZ = barrel.right.z;

                    send(Packet.serialize(packet));
                }
                else if (rHandScript.grabPinchAction.GetStateUp(SteamVR_Input_Sources.RightHand))
                {
                    cs_packet_grab packet = new cs_packet_grab();

                    packet.size = (char)Marshal.SizeOf(typeof(cs_packet_grab));
                    packet.type = (char)Packet.CS_PACKET_GRAB;
                    packet.hand = Hand.RIGHT_HAND;
                    packet.grab = false;
                    packet.pX = 0;
                    packet.pY = 0;
                    packet.pZ = 0;
                    packet.dirX = 0;
                    packet.dirY = 0;
                    packet.dirZ = 0;

                    send(Packet.serialize(packet));
                }
                if (lHandScript.grabGripAction.GetStateDown(SteamVR_Input_Sources.LeftHand))
                {
                    //Debug.Log("Left Hand Grip Action Changed");
                    if (gun.transform.GetChild(0).GetComponent<InHand>().isInLHand())
                    {
                        //Debug.Log("Gun in Left Hand");

                        cs_packet_change_hand_state packet = new cs_packet_change_hand_state();

                        packet.size = (char)Marshal.SizeOf(typeof(cs_packet_change_hand_state));
                        packet.type = (char)Packet.CS_PACKET_CHANGE_HAND_STATE;
                        packet.hand = Hand.LEFT_HAND;
                        if (objInLHand)
                        {
                            packet.state = HandState.eDEFAULT;
                            objInLHand = false;
                        }
                        else
                        {
                            packet.state = HandState.eGUN;
                            objInLHand = true;
                        }

                        send(Packet.serialize(packet));
                    }
                    else if (torchLight.transform.GetComponent<InHand>().isInLHand())
                    {
                        cs_packet_change_hand_state packet = new cs_packet_change_hand_state();

                        packet.size = (char)Marshal.SizeOf(typeof(cs_packet_change_hand_state));
                        packet.type = (char)Packet.CS_PACKET_CHANGE_HAND_STATE;
                        packet.hand = Hand.LEFT_HAND;
                        if (objInLHand)
                        {
                            packet.state = HandState.eDEFAULT;
                            objInLHand = false;
                        }
                        else
                        {
                            packet.state = HandState.eTORCHLIGHT;
                            objInLHand = true;
                        }

                        send(Packet.serialize(packet));
                    }
                    else if (jewel4Parent.activeSelf && jewel4.transform.GetComponent<InHand>().isInLHand())
                    {
                        cs_packet_change_hand_state packet = new cs_packet_change_hand_state();

                        packet.size = (char)Marshal.SizeOf(typeof(cs_packet_change_hand_state));
                        packet.type = (char)Packet.CS_PACKET_CHANGE_HAND_STATE;
                        packet.hand = Hand.LEFT_HAND;
                        if (objInLHand)
                        {
                            packet.state = HandState.eDEFAULT;
                            objInLHand = false;
                        }
                        else
                        {
                            packet.state = HandState.eGEM4;
                            objInLHand = true;
                        }

                        send(Packet.serialize(packet));
                    }
                    else if (jewel5Parent.activeSelf && jewel5.transform.GetComponent<InHand>().isInLHand())
                    {
                        cs_packet_change_hand_state packet = new cs_packet_change_hand_state();

                        packet.size = (char)Marshal.SizeOf(typeof(cs_packet_change_hand_state));
                        packet.type = (char)Packet.CS_PACKET_CHANGE_HAND_STATE;
                        packet.hand = Hand.LEFT_HAND;
                        if (objInLHand)
                        {
                            packet.state = HandState.eDEFAULT;
                            objInLHand = false;
                        }
                        else
                        {
                            packet.state = HandState.eGEM5;
                            objInLHand = true;
                        }

                        send(Packet.serialize(packet));
                    }
                }
                if (rHandScript.grabGripAction.GetStateDown(SteamVR_Input_Sources.RightHand))
                {
                    //Debug.Log("Right Hand Grip Action Changed");
                    if (gun.transform.GetChild(0).GetComponent<InHand>().isInRHand())
                    {
                        //Debug.Log("Gun in Right Hand");
                        cs_packet_change_hand_state packet = new cs_packet_change_hand_state();

                        packet.size = (char)Marshal.SizeOf(typeof(cs_packet_change_hand_state));
                        packet.type = (char)Packet.CS_PACKET_CHANGE_HAND_STATE;
                        packet.hand = Hand.RIGHT_HAND;
                        if (objInRHand)
                        {
                            packet.state = HandState.eDEFAULT;
                            objInRHand = false;
                        }
                        else
                        {
                            packet.state = HandState.eGUN;
                            objInRHand = true;
                        }

                        send(Packet.serialize(packet));
                    }
                    else if (torchLight.transform.GetComponent<InHand>().isInRHand())
                    {
                        cs_packet_change_hand_state packet = new cs_packet_change_hand_state();

                        packet.size = (char)Marshal.SizeOf(typeof(cs_packet_change_hand_state));
                        packet.type = (char)Packet.CS_PACKET_CHANGE_HAND_STATE;
                        packet.hand = Hand.RIGHT_HAND;
                        if (objInRHand)
                        {
                            packet.state = HandState.eDEFAULT;
                            objInRHand = false;
                        }
                        else
                        {
                            packet.state = HandState.eTORCHLIGHT;
                            objInRHand = true;
                        }

                        send(Packet.serialize(packet));
                    }
                    else if (jewel4.transform.GetComponent<InHand>().isInRHand())
                    {
                        cs_packet_change_hand_state packet = new cs_packet_change_hand_state();

                        packet.size = (char)Marshal.SizeOf(typeof(cs_packet_change_hand_state));
                        packet.type = (char)Packet.CS_PACKET_CHANGE_HAND_STATE;
                        packet.hand = Hand.RIGHT_HAND;
                        if (objInRHand)
                        {
                            packet.state = HandState.eDEFAULT;
                            objInRHand = false;
                        }
                        else
                        {
                            packet.state = HandState.eGEM4;
                            objInRHand = true;
                        }

                        send(Packet.serialize(packet));
                    }
                    else if (jewel5.transform.GetComponent<InHand>().isInRHand())
                    {
                        cs_packet_change_hand_state packet = new cs_packet_change_hand_state();

                        packet.size = (char)Marshal.SizeOf(typeof(cs_packet_change_hand_state));
                        packet.type = (char)Packet.CS_PACKET_CHANGE_HAND_STATE;
                        packet.hand = Hand.RIGHT_HAND;
                        if (objInRHand)
                        {
                            packet.state = HandState.eDEFAULT;
                            objInRHand = false;
                        }
                        else
                        {
                            packet.state = HandState.eGEM5;
                            objInRHand = true;
                        }

                        send(Packet.serialize(packet));
                    }
                }
            }
            if (isHost)
            {
                for (int i = 0; i < monsters.Length; ++i)
                {
                    switch (monsters[i].monsterType)
                    {
                        case MonsterType.eScorpion:
                            {
                                Scorpion scorpion = (Scorpion)monsters[i].script;
                                if (scorpion._enemyState != Scenes.AI.EnemyState.Dead)
                                {
                                    cs_packet_monster_move movePacket = new cs_packet_monster_move();
                                    movePacket.size = (char)Marshal.SizeOf(typeof(cs_packet_monster_move));
                                    movePacket.type = (char)Packet.CS_PACKET_MONSTER_MOVE;
                                    movePacket.id = i;
                                    movePacket.pX = monsters[i].monsterObject.transform.position.x;
                                    movePacket.pY = monsters[i].monsterObject.transform.position.y;
                                    movePacket.pZ = monsters[i].monsterObject.transform.position.z;
                                    movePacket.quat = Compressed_quaternion.load(monsters[i].monsterObject.transform.rotation);

                                    send(Packet.serialize(movePacket));
                                }
                                break;
                            }
                        case MonsterType.eSkeleton:
                            {
                                Skeleton skeleton = (Skeleton)monsters[i].script;
                                if (skeleton._enemyState != Scenes.AI.EnemyState.Dead)
                                {
                                    cs_packet_monster_move movePacket = new cs_packet_monster_move();
                                    movePacket.size = (char)Marshal.SizeOf(typeof(cs_packet_monster_move));
                                    movePacket.type = (char)Packet.CS_PACKET_MONSTER_MOVE;
                                    movePacket.id = i;
                                    movePacket.pX = monsters[i].monsterObject.transform.position.x;
                                    movePacket.pY = monsters[i].monsterObject.transform.position.y;
                                    movePacket.pZ = monsters[i].monsterObject.transform.position.z;
                                    movePacket.quat = Compressed_quaternion.load(monsters[i].monsterObject.transform.rotation);

                                    send(Packet.serialize(movePacket));
                                }
                                break;
                            }
                    }
                }
            }
        }
    }
}