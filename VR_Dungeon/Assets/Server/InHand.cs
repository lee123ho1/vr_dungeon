using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InHand : MonoBehaviour
{
    private bool inHand;
    private bool hand;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "LeftHand")
        {
            //Debug.Log("enter");
            inHand = true;
            hand = Hand.LEFT_HAND;
        }
        else if (other.tag == "RightHand")
        {
            inHand = true;
            hand = Hand.RIGHT_HAND;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "LeftHand")
        {
            //Debug.Log("exit");
            inHand = false;
            hand = Hand.LEFT_HAND;
        }
        else if (other.tag == "RightHand")
        {
            inHand = false;
            hand = Hand.RIGHT_HAND;
        }
    }

    public bool isInLHand()
    {
        return inHand && (hand == Hand.LEFT_HAND);
    }

    public bool isInRHand()
    {
        return inHand && (hand == Hand.RIGHT_HAND);
    }

    // Start is called before the first frame update
    void Start()
    {
        inHand = false;
    }
}