using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowMeshCenter : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(gameObject.name + " : " + transform.GetComponent<Renderer>().bounds.center.ToString("N6") + ", rotation : " + transform.rotation.ToString("N6")
            + ", position : " + transform.position.ToString("N6"));
    }
}
