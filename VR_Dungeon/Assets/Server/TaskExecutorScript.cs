using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;
using UnityEngine;

public delegate void Task();

public class TaskExecutorScript : MonoBehaviour
{
    private Queue<Task> TaskQueue = new Queue<Task>();
    private ConcurrentQueue<Task> TaskConcurrentQueue = new ConcurrentQueue<Task>();
    private object _queueLock = new object();

    // Update is called once per frame
    void Update()
    {
        //lock (_queueLock)
        //{
        //    while (TaskQueue.Count > 0)
        //        TaskQueue.Dequeue()();
        //}
        Task task;
        int count = TaskConcurrentQueue.Count;
        while (count > 0)
        {
            if (TaskConcurrentQueue.TryDequeue(out task))
            {
                task();
                --count;
            }
        }
    }

    public void ScheduleTask(Task newTask)
    {
        //lock (_queueLock)
        //{
        //    //if (TaskQueue.Count < 10000)
        //        TaskQueue.Enqueue(newTask);
        //}
        TaskConcurrentQueue.Enqueue(newTask);
    }
}
