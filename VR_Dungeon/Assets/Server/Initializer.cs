using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Initializer : MonoBehaviour
{
    private GameObject server;
    // Start is called before the first frame update
    void Start()
    {
        server = GameObject.Find("Server");
        server.GetComponent<client>().change_scene();
    }
}
