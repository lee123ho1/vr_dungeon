using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChooseStage4 : MonoBehaviour
{
    public List<GameObject> gems;
    [SerializeField] private List<AudioClip> audioClips = new List<AudioClip>();

    private AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void StageMove()
    {
        AudioClip audio;
        if (gems[0].activeInHierarchy && gems[1].activeInHierarchy)
            audio = audioClips[0];
        else
            audio = audioClips[1];

        _audioSource.clip = audio;
        _audioSource.Play();

        if (gems[0].activeInHierarchy && gems[1].activeInHierarchy)
            SceneManager.LoadScene("Final_Stage");
    }
}