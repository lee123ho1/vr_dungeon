using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FirstSceneDoorButton : MonoBehaviour
{
    public void FirstToSecondScene()
    {
        SceneManager.LoadScene("map2");
    }
}
