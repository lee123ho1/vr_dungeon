using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChooseStage3 : MonoBehaviour
{
    public void StageMove()
    {
        SceneManager.LoadScene("map5");
    }

    public void StageBack()
    {
        SceneManager.LoadScene("map3");
    }
}