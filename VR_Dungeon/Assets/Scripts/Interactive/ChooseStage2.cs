using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChooseStage2 : MonoBehaviour
{
    public void StageMove()
    {
        SceneManager.LoadScene("map4");
    }
    public void StageBack()
    {
        SceneManager.LoadScene("map3");
    }
}
