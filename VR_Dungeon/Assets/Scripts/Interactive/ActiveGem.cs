using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveGem : MonoBehaviour
{
    public GameObject gem;
    public List<Material> materials;

    private void OnCollisionEnter(Collision collision)
    {
        if (!gem.activeInHierarchy)
        {
            if (collision.gameObject.CompareTag("Stage2Gem"))
            {
                gem.SetActive(true);
                gem.gameObject.GetComponent<MeshRenderer>().material = materials[0];
            }

            if (collision.gameObject.CompareTag("Stage3Gem"))
            {
                gem.SetActive(true);
                gem.gameObject.GetComponent<MeshRenderer>().material = materials[1];
            } 
        }
    }
}
