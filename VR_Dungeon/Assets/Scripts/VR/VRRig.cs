using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VRMap
{
    public Transform vrTarget;
    public Transform rigTarget;
    public Vector3 trackingRotationOffset;

    public void Map()
    {
        rigTarget.rotation = vrTarget.localRotation * Quaternion.Euler(trackingRotationOffset);
    }
}

public class VRRig : MonoBehaviour
{
    [SerializeField] VRMap head;

    [SerializeField] private Transform _headConstraint;
    [SerializeField] private float turnSmooth;

    void FixedUpdate()
    {
        transform.forward = Vector3.Lerp(transform.forward, 
            Vector3.ProjectOnPlane(_headConstraint.up, Vector3.up).normalized, Time.deltaTime * turnSmooth);

        head.Map();
    }
}
