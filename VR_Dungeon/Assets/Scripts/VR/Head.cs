using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Head : MonoBehaviour
{
    [SerializeField] private Transform _headData;
    [SerializeField] private Transform _head;

    private Vector3 _targetPos;

    private void Awake()
    {
        _targetPos = transform.position - _head.position;
    }

    void Update()
    { 
        transform.position = _head.position + _targetPos;

        transform.RotateAround(_head.transform.position, Vector3.right, _headData.localEulerAngles.x);
        transform.RotateAround(_head.transform.position, Vector3.up, _headData.localEulerAngles.y);
        transform.RotateAround(_head.transform.position, Vector3.forward, _headData.localEulerAngles.z);
        transform.eulerAngles = _headData.localEulerAngles;
    }
}