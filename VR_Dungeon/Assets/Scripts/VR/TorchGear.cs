using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchGear : MonoBehaviour
{
    [SerializeField] private GameObject _torch;
    [SerializeField] private List<GameObject> _torches;

    [SerializeField] private GameObject _gun;
    [SerializeField] private List<GameObject> _guns;

    private MotionAnimator _motionAnimator;
    private Animator _animator;
    private Vector3 _position;
    private Vector3 _positionPrev;

    private void Start()
    {
        _motionAnimator = GetComponent<MotionAnimator>();
        _animator = GetComponent<Animator>();
        _position = _positionPrev = transform.position;
    }

    private void Update()
    {
        if (_torches[0].activeSelf || _torches[1].activeSelf)
            _torch.SetActive(false);
        else
            _torch.SetActive(true);

        if (_guns[0].activeSelf || _guns[1].activeSelf)
            _gun.SetActive(false);
        else
            _gun.SetActive(true);

        _position = transform.position;

        if ((_position - _positionPrev).magnitude < 0.001f)
        {
            _motionAnimator.enabled = false;
            _animator.SetFloat("moveSpeed", 0);
        }
        else
            _motionAnimator.enabled = true;

        _positionPrev = _position;
    }
}
