using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class VRController : MonoBehaviour
{
    [SerializeField] private float _sensitivity = 0.1f;
    [SerializeField] private float _speed = 0.05f;

    [SerializeField] private SteamVR_Action_Boolean _movePass = null;
    [SerializeField] private SteamVR_Action_Vector2 _moveValue = null;

    [SerializeField] private Transform _cameraRig = null;
    [SerializeField] private Transform _head = null;

    private CharacterController _characterController = null;
    private bool firstFrame = false;

    private void Awake()
    {
        _characterController = GetComponent<CharacterController>();
    }

    private void Start()
    {
        
    }

    private void Update()
    {
        if(!firstFrame)
        {
            _cameraRig.localPosition = Vector3.zero;
            firstFrame = true;
        }

        //HandleHeight();
        //CalculateMovement();
    }

    private void FixedUpdate()
    {
        HandleHeight();
        CalculateMovement();
    }

    private void CalculateMovement()
    {
        Vector3 movement = Vector3.zero;

        if(_movePass.GetStateUp(SteamVR_Input_Sources.Any))
            movement = Vector3.zero;

        if (_movePass.state)
        {
            //_speed += _moveValue.axis.y * _sensitivity;
            //_speed = Mathf.Clamp(_speed, -_maxSpeed, _maxSpeed);

            //movement += oritentation * (_speed * Vector3.forward) * Time.deltaTime;
            var direction = new Vector3(_moveValue.axis.x, 0, _moveValue.axis.y);
            movement = (direction.x * _head.right + direction.z * _head.forward).normalized * _speed;
            movement.y = 0f;
        }
        var gravity = Vector3.zero;
        //Debug.Log(movement);
        if (!_characterController.isGrounded)
            gravity = (-0.1f * Vector3.up);
        _characterController.Move(movement + gravity);
    }

    private void HandleHeight()
    {
        float headHeight = Mathf.Clamp(_head.localPosition.y * _cameraRig.localScale.y, 1, 10);
        _characterController.height = headHeight;

        Vector3 newCenter = Vector3.zero;
        newCenter.y = _characterController.height / 2;
        newCenter.y += _characterController.skinWidth;

        newCenter.x = _head.localPosition.x * _cameraRig.localScale.x;
        newCenter.z = _head.localPosition.z * _cameraRig.localScale.z;

        newCenter = Quaternion.Euler(0f, -transform.eulerAngles.y, 0f) * newCenter;

        _characterController.center = newCenter;
    }
}
