using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetBody : MonoBehaviour
{
    [SerializeField] private Transform HeadPosition;

    [SerializeField] private Transform Body;

    private Vector3 offset;
    private Vector3 prevPos;

    private void Start()
    {
        offset = HeadPosition.position - Body.position;
    }

    void Update()
    {
        //if ((prevPos - HeadPosition.position).magnitude > 0.15f)
        //{
        //    prevPos = HeadPosition.position;
            Body.position = new Vector3(HeadPosition.position.x, 0f, HeadPosition.position.z) + offset;
        //}
    }
}
