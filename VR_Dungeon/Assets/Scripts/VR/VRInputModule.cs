using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Valve.VR;

public class VRInputModule : BaseInputModule
{
    [SerializeField] private Camera _camera;
    [SerializeField] private SteamVR_Input_Sources _inputSources;
    [SerializeField] private SteamVR_Action_Boolean _clickAction;

    private GameObject _currentObject = null;
    private PointerEventData _data = null;

    protected override void Awake()
    {
        base.Awake();

        _data = new PointerEventData(eventSystem);
    }

    public override void Process()
    {
        _data.Reset();
        _data.position = new Vector2(_camera.pixelWidth / 2, _camera.pixelHeight / 2);

        eventSystem.RaycastAll(_data, m_RaycastResultCache);
        _data.pointerCurrentRaycast = FindFirstRaycast(m_RaycastResultCache);
        _currentObject = _data.pointerCurrentRaycast.gameObject;

        // Clear
        m_RaycastResultCache.Clear();

        // Hover
        HandlePointerExitAndEnter(_data, _currentObject);

        // Press
        if (_clickAction.GetStateDown(_inputSources))
            ProcessPress(_data);

        // Release
        if (_clickAction.GetStateUp(_inputSources))
            ProcessRelease(_data);
    }

    public PointerEventData GetData()
    {
        return _data;
    }

    private void ProcessPress(PointerEventData data)
    {
        // 레이캐스트 설정
        data.pointerPressRaycast = data.pointerCurrentRaycast;

        // 오브젝트가 닿으면 down 핸들러 호출
        GameObject newPointerPress = ExecuteEvents.ExecuteHierarchy(_currentObject, data, ExecuteEvents.pointerDownHandler);

        // down 핸들러가 없으면 클릭 핸들러를 호출해본다
        if (newPointerPress == null)
            newPointerPress = ExecuteEvents.GetEventHandler<IPointerClickHandler>(_currentObject);

        // 데이터 설정
        data.pressPosition = data.position;
        data.pointerPress = newPointerPress;
        data.rawPointerPress = _currentObject;
    }

    private void ProcessRelease(PointerEventData data)
    {
        // 포인터 up
        ExecuteEvents.Execute(data.pointerPress, data, ExecuteEvents.pointerUpHandler);

        // 클릭 핸들러 체크
        GameObject pointerUpHandler = ExecuteEvents.GetEventHandler<IPointerClickHandler>(_currentObject);

        // 존재하는지 확인
        if(data.pointerPress == pointerUpHandler)
        {
            ExecuteEvents.Execute(data.pointerPress, data, ExecuteEvents.pointerClickHandler);
        }

        // 선택된 Gameobject 초기화
        eventSystem.SetSelectedGameObject(null);

        // 데이터 초기화
        data.pressPosition = Vector2.zero;
        data.pointerPress = null;
        data.rawPointerPress = null;  
    }
}
