using UnityEngine;
using System.Collections;

public class AimLookCharacterController : MonoBehaviour {
	
	private CharacterMotor motor;
	private static bool loggedInputInfo = false;
	
	// Use this for initialization
	void Start () {
		motor = GetComponent(typeof(CharacterMotor)) as CharacterMotor;
		if (motor==null) Debug.Log("Motor is null!!");
		
		//originalRotation = transform.localRotation;
	}
	
	void Update () {
		Vector3 directionVector = Vector3.zero;
		
		try {
			directionVector = new Vector3(Input.GetAxis("Horizontal2"), Input.GetAxis("Vertical2"), 0);
		}
		catch (UnityException e) {
			if (!loggedInputInfo) {
				loggedInputInfo = true;
			}
		}
		
		if (directionVector.magnitude>1) directionVector = directionVector.normalized;
		
		directionVector = Camera.main.transform.rotation * directionVector;
		
		Quaternion camToCharacterSpace = Quaternion.FromToRotation(Camera.main.transform.forward*-1, transform.up);
		directionVector = (camToCharacterSpace * directionVector);
		
		motor.desiredFacingDirection = directionVector;
	}
}
