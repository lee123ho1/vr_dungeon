using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camLock : MonoBehaviour
{
    public Transform cam;
    public Transform player;

    private Vector3 offSet;

    private void Start()
    {
        offSet = cam.position - player.position;
        
    }

    // Update is called once per frame
    void Update()
    {
        cam.position = player.position + offSet;
    }
}
