﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class LockToPoint : MonoBehaviour
{
    [SerializeField] public Transform snapTo;
    [SerializeField] private float snapTime = 2;

    private Rigidbody _body;
    private Interactable _interactable;
    private float _dropTimer;

    private void Start()
    {
        _interactable = GetComponent<Interactable>();
        _body = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if (snapTo != null)
        {
            bool used = false;
            if (_interactable != null)
                used = _interactable.attachedToHand;

            if (used)
            {
                _body.isKinematic = false;
                _dropTimer = -1;
            }
            else
            {
                _dropTimer += Time.deltaTime / (snapTime / 2);

                _body.isKinematic = _dropTimer > 1;

                if (_dropTimer > 1)
                {
                    //transform.parent = snapTo;
                    transform.position = snapTo.position;
                    transform.rotation = snapTo.rotation;
                }
                else
                {
                    float t = Mathf.Pow(35, _dropTimer);

                    _body.velocity = Vector3.Lerp(_body.velocity, Vector3.zero, Time.fixedDeltaTime * 4);
                    if (_body.useGravity)
                        _body.AddForce(-Physics.gravity);

                    transform.position = Vector3.Lerp(transform.position, snapTo.position, Time.fixedDeltaTime * t * 3);
                    transform.rotation = Quaternion.Slerp(transform.rotation, snapTo.rotation, Time.fixedDeltaTime * t * 2);
                }
            } 
        }
    }
}